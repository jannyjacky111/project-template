<?php

namespace app\api\pub\v1;

use Yii;
use yii\base\BootstrapInterface;
/**
 * v1 module definition class
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\api\pub\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->user->enableSession = false;

    }

    public function bootstrap($app)
    {
        Yii::$app->getUrlManager()->addRules(require(__DIR__ . '/paths.php'), false);
    }
}
