<?php

/**
 * @SWG\Get(
 *     path="/pets/findByStatus",
 *     summary="Finds Pets by status",
 *     description="Multiple status values can be provided with comma separated strings",
 *     operationId="findPetsByStatus",
 *     produces={"application/xml", "application/json"},
 *     tags={"pet"},
 *     @SWG\Parameter(
 *         name="status",
 *         in="query",
 *         description="Status values that need to be considered for filter",
 *         required=true,
 *         type="array",
 *         @SWG\Items(
 *             type="string",
 *             enum={"available", "pending", "sold"},
 *             default="available"
 *         ),
 *         collectionFormat="multi"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *         @SWG\Schema(
 *             type="array",
 *             @SWG\Items($ref="#/definitions/Pet")
 *         ),
 *     ),
 *     @SWG\Response(
 *         response="400",
 *         description="Invalid status value",
 *     )
 * )
 *
 * @SWG\Get(
 *     path="/pets/{petId}",
 *     summary="Find pet by ID",
 *     description="Returns a single pet",
 *     operationId="getPetById",
 *     tags={"pet"},
 *     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         description="ID of pet to return",
 *         in="path",
 *         name="petId",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *         @SWG\Schema($ref="#/definitions/Pet")
 *     ),
 *     @SWG\Response(
 *         response="400",
 *         description="Invalid ID supplied"
 *     ),
 *     @SWG\Response(
 *         response="404",
 *         description="Pet not found"
 *     )
 * )
 *
 * @SWG\Post(
 *     path="/pets",
 *     tags={"pet"},
 *     operationId="addPet",
 *     summary="Add a new pet to the store",
 *     description="",
 *     consumes={"application/json", "application/xml"},
 *     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @SWG\Schema($ref="#/definitions/Pet"),
 *     ),
 *     @SWG\Response(
 *         response=405,
 *         description="Invalid input",
 *     )
 * )
 *
 * @SWG\Put(
 *     path="/pets",
 *     tags={"pet"},
 *     operationId="updatePet",
 *     summary="Update an existing pet",
 *     description="",
 *     consumes={"application/json", "application/xml"},
 *     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         name="body",
 *         in="body",
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @SWG\Schema($ref="#/definitions/Pet"),
 *     ),
 *     @SWG\Response(
 *         response=400,
 *         description="Invalid ID supplied",
 *     ),
 *     @SWG\Response(
 *         response=404,
 *         description="Pet not found",
 *     ),
 *     @SWG\Response(
 *         response=405,
 *         description="Validation exception",
 *     )
 * )
 *
 * @SWG\Delete(
 *     path="/pets/{petId}",
 *     summary="Deletes a pet",
 *     description="",
 *     operationId="deletePet",
 *     produces={"application/xml", "application/json"},
 *     tags={"pet"},
 *     @SWG\Parameter(
 *         description="Pet id to delete",
 *         in="path",
 *         name="petId",
 *         required=true,
 *         type="integer",
 *         format="int64"
 *     ),
 *     @SWG\Parameter(
 *         name="api_key",
 *         in="header",
 *         required=false,
 *         type="string"
 *     ),
 *     @SWG\Response(
 *         response=400,
 *         description="Invalid ID supplied"
 *     ),
 *     @SWG\Response(
 *         response=404,
 *         description="Pet not found"
 *     )
 * )
 *
 * @SWG\Security(
 *    petAuth="[]"
 * )
 *
 * @SWG\Definition(
 *     name="Pet",
 *     type="object",
 *     required={"name"},
 *     @SWG\Property(
 *          name="name",
 *          type="string"
 *     ),
 *     @SWG\Property(
 *          name="photoUrl",
 *          type="string"
 *     ),
 *     @SWG\Property(
 *          name="age",
 *          type="integer"
 *     )
 * )
 *
 * @SWG\SecurityDefinition(
 *     title="petAuth",
 *     type="apiKey",
 *     description="api key security",
 *     name="pet_auth",
 *     in="query"
 * )
 *
 */
class PublicCest
{

}