<?php

namespace app\api\components;

class Helper
{

    //хелпер
    public static function errorsToStr ($errorsArray)
    {
        $errors = array();

        if(!empty($errorsArray)) {

            foreach ($errorsArray as $fieldErrors) {
                array_push($errors, implode(';', $fieldErrors));
            }

            return implode(' ', $errors);
        }else {
            return '';
        }
    }
}