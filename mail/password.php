<tr>
    <td style="color: #262626;border:none;">
        <?= $name;?>, Ваш пароль был успешно изменен!
    </td>
</tr>
<tr>
    <td>
        <table cellpadding="20" cellspacing="0" border="0" width="100%" style="background: rgba(78, 132, 255, 0.05);">
            <tr>
                <td style="color: #262626;border:none;">
                    Регистрационные данные:<br>
                    Ваш логин: <b><a style="color: #262626;"><?= $username;?></a></b><br>
                    Ваш пароль: <b><?= $password;?></b>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td style="color: #262626;border:none;line-height: 1.5;">
        Успехов, <?= $name;?>!<br>
        Команда ViaFit.
    </td>
</tr>
