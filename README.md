DIRECTORY STRUCTURE
-------------------

      admin/
           assets/               contains assets definition
           components/           contains custom components for views
           controllers/          contains Web controller classes
           models/               contains model classes
           views/                contains view files
      api/                       contains api folders
      commands/                  contains console commands (controllers)
      commom/models/             contains commom models (Notification, Sms, WebSocket)
      config/                    contains application configurations
      front/                     contains automatic api documentation controller
      mail/                      contains view files for e-mails
      messages/                  contains dictionaries for application
      runtime/                   contains files generated during runtime
      vendor/                    contains dependent 3rd-party packages
      web/                       contains the entry script and Web resources


INSTALLATION
------------

### Install

You can then install this project template using the following commands:

Extract the archive file downloaded from repo to
a directory or clone it by git

~~~
git clone repo.git path/to/folder
~~~

Now you should run a script in the root directory of application.
It creates admin catalog (if it is need for your projects) and api catalogs, names of which you should enter.

~~~
php RSInstaller.php
~~~

Now you should install composer

~~~
composer install
~~~

You can check installation.
~~~
php yii serve
~~~
You can then access the admin part of application through the following URL:
~~~
http://localhost:8080/admin
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

### Params

Edit the file 'config/params.php'

```php

   return [
       'adminEmail' => ['admin@example.com' => 'project-name'], //if you will send email
       'sms'=> [                                                //if you will send sms
           'CompanyName' => 'SMS-INFO',
           'AccountId' => '',
           'AccountKey' => '',
       ],
       'firebaseApiKey' => ''                                   //if you will send push-notification
   ];
```

COMMANDS
--------

### Automatic documentation

You can generate automatic docs for your api by using SwaggerController. It finds comments in swagger format in files of
folder {directoryPath} and generate yaml file {resultYamlPath}.

~~~
php yii swagger {directoryPath} {resultYamlPath}
~~~

Try run command bellow to see documentation example.

~~~
php yii swagger api/pub/v1/tests/api web/files/public-test-v1.yml
~~~
You can then access the documentation of api through the following URL:
~~~
http://localhost:8080/docs/public-test-v1
~~~

### Cron settings

You should add cron jobs in CronController and run following command:
~~~
php yii cron
~~~

### Internationalization

With the help of TranslationController you can automatic add phrases in Yii::t() and model labels into dictionary file.


COMMON MODELS
-------------

### Notification

Lets your application send push notifications to web, android or ios applications with firebase.

### Sms

Lets your application send sms.

TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2_basic_tests` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run -- --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit -- --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit -- --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.
