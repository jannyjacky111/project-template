<?php

$config = require(__DIR__ . '/config-common.php');

//дополняем ID приложения идентификатором
$config['id'] = $config['id'].' [api]';

//для того чтобы API мог принимать данные в формате JSON
$config['components']['request'] = [
    'parsers' => [
        'application/json' => 'yii\web\JsonParser',
    ]
];

$config['components']['response'] = [
    'formatters' => [
        \yii\web\Response::FORMAT_JSON => [
            'class' => 'yii\web\JsonResponseFormatter',
            'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
            'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
        ],
    ],
    'format' =>  \yii\web\Response::FORMAT_JSON

];

return $config;