<?php

$config = require(__DIR__ . '/config-common.php');

//дополняем ID приложения идентификатором
$config['id'] = $config['id'].' [console]';

//для консольного приложения свои настройки контроллеров
$config['controllerNamespace'] = 'app\commands';

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;