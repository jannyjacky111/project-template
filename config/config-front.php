<?php

$config = require(__DIR__ . '/config-common.php');

//дополняем ID приложения идентификатором
$config['id'] = $config['id'].' [front]';

//
$config['components']['request'] = [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => 'pBSdxyPPF6lI4QtsadfA3jCXxM8PcjbgbpD_',
];

$config['layoutPath'] = '@app/front/views/layouts';
$config['viewPath'] = '@app/front/views';

//для консольного приложения свои настройки контроллеров
$config['controllerNamespace'] = 'app\front\controllers';

$congig['components']['assetManager'] = [
    'forceCopy' => true,
];

return $config;