<?php

$config = require(__DIR__ . '/config-api.php');


//каждый модуль версия API
$config['modules'] = [
    'v1' => [
        'class' => 'app\api\pub\v1\Module',
    ],
];

$config['bootstrap'][] = 'v1';

$config['components']['user'] = [
    'identityClass' => false
];

return $config;