<?php
$db = require(__DIR__ . '/db/db.php');
$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'back-end',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'components' => [
//        'cache' => [
//            'class' => 'yii\caching\FileCache',
//        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true, //доступ только к тем правилам которые настроены
            'showScriptName' => false,
            'rules' => [
	            //TODO: перенести в конфиг фронта
	            'docs/<version>' => 'docs/index'
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //TODO: сменить почту
            'useFileTransport' => true,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => '',
//                'password' => '',
//                'port' => '587',
//                'encryption' => 'tls',
//            ],
            'htmlLayout' => '@app/mail/layouts/html'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['test_category'],
                    'logFile' => '@app/runtime/logs/test_success.log',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app' => 'dictionary.php'
                    ]
                ],
            ],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'defaultTimeZone' => 'Europe/Kaliningrad',
            'timeZone' => 'GMT+2',
            'datetimeFormat' => 'php:d.m.Y G:i',
            'dateFormat' => 'd.MM.Y',
            'timeFormat' => 'php:G:i'
        ],
        'db' => $db
    ],
    'aliases' => [
        '@imgDomen' => 'http://dev.vfdev.ga',
    ],
    'params' => $params
];


if (YII_ENV_DEV) {
    $config['components']['assetManager'] = [
        //'forceCopy' => true,
        'forceCopy' => false,

    ];
}


return $config;