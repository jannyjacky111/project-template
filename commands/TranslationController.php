<?php
namespace app\commands;

use yii\console\Controller;

/**
 * Class TranslationController
 * @package app\commands
 * 
 * helps to find phrases which are need a translation
 */
class TranslationController extends Controller
{

    /**
     * @param string $modelsPath - path to models
     * @param string $fileSuffix
     * @param string $dictionaryPath - path to dictionary file
     * 
     * adds labels to dictionary file
     */
    public function actionLabels($modelsPath = 'tables', $fileSuffix = '.php', $dictionaryPath = 'messages/ru-RU/dictionary.php')
    {
        $labels = file_exists($dictionaryPath) ? require($dictionaryPath) : [];
        if (!is_array($labels)) $labels = [];
        $file = fopen($dictionaryPath, 'w');

        $content = "<?php return [\n";

        foreach (scandir($modelsPath) as $model) {
            if (stripos($model, $fileSuffix) !== false) {
                $modelClass = substr($model, 0, stripos($model,'.'));
                $modelClass = 'app\\'.$modelsPath.'\\'.$modelClass;
                foreach ((new $modelClass)->attributeLabels() as $attribute => $label) {
                    if (!isset($labels['['.$label.']'])) $labels['['.$label.']'] = '';
                }
            }

        }
        foreach ($labels as $label => $translation) $content .= "\t'".$label."' => '".$translation."',\n";
        $content .= "];";
        file_put_contents($dictionaryPath, $content);
        fclose($file);
    }

    /**
     * @param string $path - folder where method searches phrases for translation
     * @param string $fileSuffix
     * @param string $dictionaryPath
     * 
     * adds to dictionary phrases in Yii::t(...)
     */
    public function actionMessages($path = '', $fileSuffix = '.php', $dictionaryPath = 'messages/ru-RU/dictionary.php')
    {
        $messages = file_exists($dictionaryPath) ? require($dictionaryPath) : [];
        if (!is_array($messages)) $messages = [];
        $files = $this->getFiles($path, [], $fileSuffix);
        $dictionaryFile = fopen($dictionaryPath, 'w');
        $content = "<?php return [\n";
        foreach ($files as $file) {
            $fileContent = file_get_contents($file);
            while (strlen($fileContent) > 0) {
                $start = stripos($fileContent, 'Yii::t(');
                if ($start !== false) {
                    $start = stripos($fileContent, ',', $start) + 1;
                    $end = stripos($fileContent, ')', $start);
                    $message = trim(substr($fileContent, $start, $end - $start));
                    $message = substr($message, 1, strlen($message) - 2);
                    if (!isset($messages[$message]))  $messages[$message] = '';
                    $fileContent = substr($fileContent, $end + 1);
                } else $fileContent = '';
            }
        }
        foreach ($messages as $message => $translation) $content .= "\t\"".$message."\" => '".$translation."',\n";
        $content .= "];";
        file_put_contents($dictionaryPath, $content);
        fclose($dictionaryFile);
    }

    /**
     * @param $folder
     * @param array $files
     * @param null $fileSuffix
     * @return array
     *  
     * gets all files in a folder recursively
     */
    private function getFiles($folder, $files = [], $fileSuffix = null)
    {
        $scanResults = scandir($folder);
        foreach ($scanResults as $scanResult) {
            if (is_file($folder.'/'.$scanResult)) {
                if (stripos($scanResult, $fileSuffix) === false) continue;
                $files[] = $folder.'/'.$scanResult;
            }
            else if (stripos($scanResult, '.') === false) {
                $files = array_unique(array_merge($this->getFiles($folder.'/'.$scanResult, $files, $fileSuffix), $files));
            }
        }
        return $files;
    }
}