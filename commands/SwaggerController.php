<?php
namespace app\commands;

use yii\console\Controller;

/**
 * Class SwaggerController
 * Контроллер для сбора документации по спецификации Swagger 
 * 
 * ЗАПУСК: php yii swagger {directoryPath} {resultYamlPath}
 * параметры являются обязательными
 * {directoryPath} - путь к директории, из файлов которой необходими собрать документацию
 * {resultYamlPath} - путь к файлу *.yml, в который необходимо сохранить результат
 * 
 * при запуске команды из консоли контроллер собирает комментарии определенного вида по всем файлам заданной папки в массив,
 * преобразовывает его для записи в yaml файл и записывает 
 * 
 * СОБИРАЕМЫЙ ВИД КОММЕНТАРИЕВ
 * ключевая строка @SWG\{SwaggerObject}(...)
 *
 * для теста можно воспользоваться тестовым комментарием ниже
 *
 * СПЕЦИФИКАЦИЯ SWAGGER https://swagger.io/specification/#securityDefinitionsObject
 * 
 * Проверить полученный yaml файл на валидность можно вставив его содержимое в editor:
 * http://editor.swagger.io/
 * 
 * @package app\commands
 */
 /**
 * ТЕСТОВЫЙ КОММЕНТАРИЙ
  * @SWG\Get(
  *     path="/pets/findByStatus",
  *     summary="Finds Pets by status",
  *     description="Multiple status values can be provided with comma separated strings",
  *     operationId="findPetsByStatus",
  *     produces={"application/xml", "application/json"},
  *     tags={"pet"},
  *     @SWG\Parameter(
  *         name="status",
  *         in="query",
  *         description="Status values that need to be considered for filter",
  *         required=true,
  *         type="array",
  *         @SWG\Items(
  *             type="string",
  *             enum={"available", "pending", "sold"},
  *             default="available"
  *         ),
  *         collectionFormat="multi"
  *     ),
  *     @SWG\Response(
  *         response=200,
  *         description="successful operation",
  *         @SWG\Schema(
  *             type="array",
  *             @SWG\Items($ref="#/definitions/Pet")
  *         ),
  *     ),
  *     @SWG\Response(
  *         response="400",
  *         description="Invalid status value",
  *     )
  * )
  *
  * @SWG\Get(
  *     path="/pets/{petId}",
  *     summary="Find pet by ID",
  *     description="Returns a single pet",
  *     operationId="getPetById",
  *     tags={"pet"},
  *     produces={"application/xml", "application/json"},
  *     @SWG\Parameter(
  *         description="ID of pet to return",
  *         in="path",
  *         name="petId",
  *         required=true,
  *         type="integer",
  *         format="int64"
  *     ),
  *     @SWG\Response(
  *         response=200,
  *         description="successful operation",
  *         @SWG\Schema($ref="#/definitions/Pet")
  *     ),
  *     @SWG\Response(
  *         response="400",
  *         description="Invalid ID supplied"
  *     ),
  *     @SWG\Response(
  *         response="404",
  *         description="Pet not found"
  *     )
  * )
  *
  * @SWG\Post(
  *     path="/pets",
  *     tags={"pet"},
  *     operationId="addPet",
  *     summary="Add a new pet to the store",
  *     description="",
  *     consumes={"application/json", "application/xml"},
  *     produces={"application/xml", "application/json"},
  *     @SWG\Parameter(
  *         name="body",
  *         in="body",
  *         description="Pet object that needs to be added to the store",
  *         required=true,
  *         @SWG\Schema($ref="#/definitions/Pet"),
  *     ),
  *     @SWG\Response(
  *         response=405,
  *         description="Invalid input",
  *     )
  * )
  *
  * @SWG\Put(
  *     path="/pets",
  *     tags={"pet"},
  *     operationId="updatePet",
  *     summary="Update an existing pet",
  *     description="",
  *     consumes={"application/json", "application/xml"},
  *     produces={"application/xml", "application/json"},
  *     @SWG\Parameter(
  *         name="body",
  *         in="body",
  *         description="Pet object that needs to be added to the store",
  *         required=true,
  *         @SWG\Schema($ref="#/definitions/Pet"),
  *     ),
  *     @SWG\Response(
  *         response=400,
  *         description="Invalid ID supplied",
  *     ),
  *     @SWG\Response(
  *         response=404,
  *         description="Pet not found",
  *     ),
  *     @SWG\Response(
  *         response=405,
  *         description="Validation exception",
  *     )
  * )
  *
  * @SWG\Delete(
  *     path="/pets/{petId}",
  *     summary="Deletes a pet",
  *     description="",
  *     operationId="deletePet",
  *     produces={"application/xml", "application/json"},
  *     tags={"pet"},
  *     @SWG\Parameter(
  *         description="Pet id to delete",
  *         in="path",
  *         name="petId",
  *         required=true,
  *         type="integer",
  *         format="int64"
  *     ),
  *     @SWG\Parameter(
  *         name="api_key",
  *         in="header",
  *         required=false,
  *         type="string"
  *     ),
  *     @SWG\Response(
  *         response=400,
  *         description="Invalid ID supplied"
  *     ),
  *     @SWG\Response(
  *         response=404,
  *         description="Pet not found"
  *     )
  * )
  *
  * @SWG\Security(
  *    petAuth="[]"
  * )
  *
  * @SWG\Definition(
  *     name="Pet",
  *     type="object",
  *     required={"name"},
  *     @SWG\Property(
  *          name="name",
  *          type="string"
  *     ),
  *     @SWG\Property(
  *          name="photoUrl",
  *          type="string"
  *     ),
  *     @SWG\Property(
  *          name="age",
  *          type="integer"
  *     )
  * )
  *
  * @SWG\SecurityDefinition(
  *     title="petAuth",
  *     type="apiKey",
  *     description="api key security",
  *     name="pet_auth",
  *     in="query"
  * )
  *
 */
class SwaggerController extends Controller
{

    public $defaultAction = 'make';
    /**
     * @var string
     * строка для записи в yaml файл
     */
    public $ymlContent = '';

    /**
     * @var array
     * массив с данными swagger, подготавливаемыми для записи в yaml файл
     */
    public $swaggerArray = [];

    /**
     * @var array
     * массив ключей параметров, которые в yaml файле должны быть записаны как строка
     */
    private $stringType = [
        'swagger', 'version', '$ref'
    ];

    //паттерн для имени переменной
    private $pattern = ['/\/\*/', '/\*\//', '/\*/', "/\t/", '/ /', "/\n/", "/\"/"];
    //паттерн для строк в комментариях, содержащих значение
    private $patternVal = ['/\/\*/', '/\*\//', '/\*/', "/\t/", "/\n/", "/\"/"];
    private $replace = ['', '', '', '', '', '', ''];

    /**
     * инициализация основных данных массива $swaggerArray
     *
     * массив построен по спецификации Swagger
     * swagger, info, paths - обязательные поля
     *
     * swagger - используемая версия swagger, должна быть "2.0"
     * info - основная информация о проекте
     *      title и version - обязательные поля массива $info
     *
     *      title - заголовок
     *      description - краткое описание приложения
     *      termsOfService - определения сервиса
     *      contacts - контактная информация для API
     *      license - информация по лицензионном соглашении API
     *      version - версия API
     * host - имя или ip-адрес хоста
     * schemes - протокол передачи данных, http или https
     * basePath - основной путь, относительно хоста
     * produces - списоктипов, которые api может производить
     * definitions - типы данных, отдаваемых и получаемых API
     * paths - доступные точки входа API
     * responses - возможные ответы API
     * securityDefinitions - определения возможных типов авторизации для API
     * security - объявление авторизации, примененной к данному API
     * externalDocs - дополнительная документация
     */
    public function init()
    {
        $this->swaggerArray = [
            'swagger' => "2.0",
            'info' => [
                'title' => "Project API",
                'description' => "automatic API for project",
                'termsOfService' => "http://localhost:8080/",
                'contact' => [
                    'name' => "Контакты",
                    'url' => "http://localhost:8080/",
                    'email' => "test@mail.ru"
                ],
                'license' => [
                    'name' => "Пользовательское соглашение",
                    'url' => "http://localhost:8080/"
                ],
                'tags' => [
                    ['name' => 'auth'],
                    ['name' => 'me'],
                ],
                'version' => "1.0"
            ],
            'host' => "localhost",
            'schemes' => ["http"],
            'basePath' => "/api/client/v1",
            'produces' => ["application/json"],
            'security' => [],
            'paths' => [],
            'definitions' => [],
            'parameters' => [],
            'responses' => [],
            'securityDefinitions' => [],
            'externalDocs' => []
        ];
        parent::init();
    }

    /**
     * @param $folder - путь к папке с файлами, в которых содержится документация
     * @param $filename - путь для сохранения yaml файла
     *
     * действие сканирует указанный каталог и собирает из его файлов документацию,
     * преобразует в yml и записывает в созданный yaml файл
     */
    public function actionMake($folder, $filename)
    {
        if (!file_exists($folder)) {
            print_r("не найден каталог по указанному пути: ".$folder."\n");
            exit();
        }
        if (!stripos($filename, '.yml')) {
            print_r($filename." должно являться путем к yaml файлу\n");
            exit();
        }
        self::init();
        $file = fopen($filename, 'w');
        $this->swaggerArray = array_merge($this->swaggerArray, $this->scanFolder($folder));
        $this->ymlContent .= $this->arrayToYaml($this->swaggerArray);
        file_put_contents($filename, $this->ymlContent);
        fclose($file);
    }


    /**
     * @param $swaggerArray - массив swagger данных
     * @param $content - строка для yaml файла
     * @param $depth - глубина вложенности массива, на которой программа находится на текущем шаге
     * @return string - возвращает подготовленную для записи в yaml файл строку
     */
    private function arrayToYaml($swaggerArray, $content = "", $depth = 0)
    {
        foreach ($swaggerArray as $swaggerKey => $swaggerItem) {
            if (empty($swaggerItem)) continue;
            if (is_array($swaggerItem)) {
                $content .= str_repeat(" ", 4*$depth).(is_int($swaggerKey) ? "-   " : trim($swaggerKey, '"').":\n");
                $depth++;
                $content = $this->arrayToYaml($swaggerItem, $content, $depth);
                $depth--;
            } else {
                if (is_int($swaggerKey)) {
                    $content .= str_repeat(" ", 4*$depth).'-   '.$swaggerItem."\n";
                } else {
                    if (!preg_match("/\n$/", $content)) $content .= trim($swaggerKey, '"').": ";
                    else $content .= str_repeat(" ", 4*$depth).trim($swaggerKey, '"').": ";
                    if (in_array($swaggerKey, $this->stringType)) $content .= "\"".$swaggerItem."\"\n";
                    else $content .= $swaggerItem."\n";
                }

            }
        }
        return $content;
    }

    /**
     * @param $folder - путь к папке
     * @return array - возвращает массив swagger данных, найденных в фвйлах каталога $folder
     */
    private function scanFolder($folder)
    {
        $files = $this->getFiles($folder, []);
        $swaggerArray = [];
        foreach ($files as $file) {
            $swaggerArray = array_merge($swaggerArray, $this->scanFile($file, $swaggerArray));
        }
        return $swaggerArray;
    }

    /**
     * @param $folder - каталог для поиска
     * @param $files - массив найденных файлов
     * @return array - возвращает массив найденных в каталоге и во всех внутренних каталогах файлов,
     * не являющихся yaml файлами и вспомогательными файлами
     */
    private function getFiles($folder, $files)
    {
        $scanResults = scandir($folder);
        foreach ($scanResults as $scanResult) {
            if (stripos($scanResult, '_') === 0) continue;
            if (is_file($folder.'/'.$scanResult)) {
                if (stripos($scanResult, '.yml') !== false) continue;
                $files[] = $folder.'/'.$scanResult;
            }
            else if (stripos($scanResult, '.') === false) {
                $files = array_unique(array_merge($this->getFiles($folder.'/'.$scanResult, $files), $files));
            }
        }
        return $files;
    }

    /**
     * @param $file - путь к файлу
     * @param $swaggerArray - массив с данными swagger
     * @return array - возвращает массив, дополненный данными swagger из файла $file
     */
    private function scanFile($file, $swaggerArray)
    {
        $comments = $this->getComments($file);
        foreach ($comments as $comment){
            $swaggerArray = array_merge($swaggerArray, $this->scanComment($comment, $swaggerArray));
        }
        return $swaggerArray;
    }

    /**
     * @param $file - путь к файлу
     * @return array - массив комментариев
     *
     * функция возвращает массив комментариев из файла
     */
    private function getComments($file)
    {
        $comments = [];
        $content = file_get_contents($file);
        while (strlen($content) > 0) {
            $firstPos = stripos($content, '/*');
            $lastPos = stripos($content, '*/', $firstPos);

            $comments[] = substr($content, $firstPos, $lastPos - $firstPos);
            $content = substr($content, $lastPos + 1);
        }
        return $comments;
    }

    /**
     * @param $comment - текст комментария
     * @param $swaggerArray - массив с данными swagger
     * @return mixed
     *
     * функция дополняет массив с данными swagger swag-данными из комментария $comment,
     * если они там есть
     */
    private function scanComment($comment, $swaggerArray)
    {
        $swaggerKey = $this->getSwaggerKey($comment);
        if ($swaggerKey === '') {
            $finalArrays = explode(',', $comment);
            foreach ($finalArrays as $finalArray) {
                $finalArray = explode('=', $finalArray);
                if (count($finalArray) > 1) $swaggerArray[$finalArray[0]] = $finalArray[1];
            }
            return $swaggerArray;
        }
        $currentInnerComment = $this->getInnerComment($comment);
        $swaggerArray = array_merge($swaggerArray, $this->addSwaggerObject($currentInnerComment, $swaggerKey, $swaggerArray));
        $comment = substr($comment, stripos($comment,$currentInnerComment) + strlen($this->getInnerComment($comment)));
        $swaggerArray = array_merge($swaggerArray, $this->scanComment($comment, $swaggerArray));
        return $swaggerArray;
    }

    /**
     * @param $comment - комментарий
     * @param $swaggerKey - название swagger объекта
     * @param $swaggerArray - массив с данными swagger
     *
     * дополняет массив $swaggerArray информацией из комментария $comment
     * по схеме, зависящей от типа swagger объекта $swaggerKey
     */
    private function addSwaggerObject($comment, $swaggerKey, $swaggerArray)
    {
        switch ($swaggerKey) {
            case 'Definition':
                $result = $this->makeResultArrayFromComment($comment, "name");
                $swaggerArray["definitions"][$result['name']] = $result['resultArray'];
                break;
            case 'Property':
                $result = $this->makeResultArrayFromComment($comment, "name");
                $swaggerArray["properties"][$result['name']] = $result['resultArray'];
                break;
            case 'Get':
            case 'Put':
            case 'Delete':
            case 'Options':
            case 'Post':
                $result = $this->makeResultArrayFromComment($comment, "path");
                $swaggerArray["paths"][$result['name']][strtolower($swaggerKey)] = $result['resultArray'];
                break;
            case 'Parameter':
                $swaggerArray["parameters"][] = $this->makeResultArrayFromComment($comment);
                break;
            case 'Response':
                $result = $this->makeResultArrayFromComment($comment, "response");
                $swaggerArray["responses"]['"'.$result['name'].'"'] = $result['resultArray'];
                break;
            case 'Security':
                $swaggerArray['security'][] = $this->makeResultArrayFromComment($comment);
                break;
            case 'SecurityDefinition':
                $result = $this->makeResultArrayFromComment($comment, "title");
                $swaggerArray['securityDefinitions'][$result['name']] = $result['resultArray'];
                break;
            default:
                if (!isset($swaggerArray[strtolower($swaggerKey)])) $swaggerArray[strtolower($swaggerKey)] = [];
                $swaggerArray[strtolower($swaggerKey)] = array_merge($swaggerArray[strtolower($swaggerKey)], $this->makeResultArrayFromComment($comment));
        }

        return $swaggerArray;
    }

    /**
     * @param $comment - комментарий
     * @param string $nameAttribute - параметр массива в комментарии,
     * который добавится в результирующий массив как ключ
     *
     * если $nameAttribute указан, возвращает массив вида
     * [ 'resultArray' => массив, собранный из комментария,
     *   'name' => новый ключ массива (значение аттрибута $nameAttribute)],
     * иначе возвращает массив, собранный из комментария
     *
     * @return array
     */
    private function makeResultArrayFromComment($comment, $nameAttribute = '')
    {
        $array = $this->makeArrayFromComment($comment);
        $resultArray = [];
        $name = '';
        foreach ($array as $item) {
            $commentSwaggerKey = $this->getSwaggerKey($item);
            if ($commentSwaggerKey === '') {
                $finalArray = explode('=', $item);
                if (count($finalArray) > 1) {
                    $finalArray[0] = preg_replace($this->pattern, $this->replace, trim($finalArray[0]));
                    if (stripos($finalArray[1], '{') === 0) {
                        $finalArray[1] = json_decode(preg_replace(['/{/', '/}/'], ['[', ']'], $finalArray[1]));
                    } else $finalArray[1] = preg_replace($this->patternVal, $this->replace, trim($finalArray[1]));

                    if ($nameAttribute && $finalArray[0] == $nameAttribute) $name = $finalArray[1];
                    else $resultArray[$finalArray[0]] = $finalArray[1];
                }
            } else {
                $resultArray = array_merge($resultArray, $this->addSwaggerObject($this->getInnerComment($item), $commentSwaggerKey, $resultArray));
            }
        }
        if ($nameAttribute) return ['resultArray' => $resultArray, 'name' => $name];
        else return $resultArray;
    }


    /**
     * @param $comment
     * подготавливает массив комментариев из комментария
     * (@SWG объекты становятся отдельными элементами результирующего массива)
     *
     * Пример:
     * комментарий
     * @SWG\Definition(
     *   name="Admin",
     *   @SWG\Property(title="username", format="string", description="user's login")
     * )
     * результат
     * [ name="Admin",
     *   @SWG\Property(title="username", format="string", description="user's login") ]
     *
     * @return array
     */
    private function makeArrayFromComment($comment)
    {
        $exploder = ",\n";
        $array = [];
        $commentSwaggerKey = $this->getSwaggerKey($comment);
        while ($commentSwaggerKey !== '') {
            $currentInnerComment = $this->getInnerComment($comment);
            $array[] = '@SWG\\'.$commentSwaggerKey.'('.$currentInnerComment.')';
            $comment = substr($comment, 0, stripos($comment,'@SWG')).substr($comment, stripos($comment, ')', stripos($comment,$currentInnerComment) + strlen($this->getInnerComment($comment))) + 1);
            $commentSwaggerKey = $this->getSwaggerKey($comment);
        }
        return array_merge(explode($exploder, $comment), $array);
    }

    /**
     * @param $comment
     * возвращает название внешнего swagger объекта, описанного в комментарии $comment
     * если внутри комментария нет swagger объекта, возвращает пустую строку
     *
     * Пример
     * комментарий
     * name="Pet",
     * type="object",
     * required={"name"},
     * @SWG\Property(
     *    name="name",
     *    type="string"
     * ),
     * @SWG\Property(
     *     name="photoUrl",
     *     type="string"
     * ),
     * @SWG\Property(
     *     name="age",
     *     type="integer"
     * )
     *
     * результат
     * Property
     *
     * @return string
     */
    public function getSwaggerKey($comment)
    {
        $swgPos = stripos($comment, '@SWG\\');
        if ($swgPos === false) return '';
        return substr($comment, stripos($comment, '\\', $swgPos) + 1, stripos($comment, '(', $swgPos) - stripos($comment, '\\', $swgPos) - 1);
    }

    /**
     * @param $comment
     * возвращает часть комментария внутри внешнего swagger объекта
     * если внутри комментария нет swagger объекта, возвращает пустую строку
     *
     * Пример:
     * комментарий
     * @SWG\Definition(
     *     name="Pet",
     *     type="object",
     *     required={"name"},
     *     @SWG\Property(
     *          name="name",
     *          type="string"
     *     ),
     *     @SWG\Property(
     *          name="photoUrl",
     *          type="string"
     *     ),
     *     @SWG\Property(
     *          name="age",
     *          type="integer"
     *     )
     * )
     * Результат
     * name="Pet",
     * type="object",
     * required={"name"},
     * @SWG\Property(
     *    name="name",
     *    type="string"
     * ),
     * @SWG\Property(
     *     name="photoUrl",
     *     type="string"
     * ),
     * @SWG\Property(
     *     name="age",
     *     type="integer"
     * )
     *
     * @return string
     */
    public function getInnerComment($comment)
    {
        $firstPos = stripos($comment, '@SWG\\');
        if ($firstPos === false) return '';
        $leftCnt = 0;
        $rightCnt = 0;
        for ($i = $firstPos + 4; $i < strlen($comment); $i++) {
            if ($comment[$i] == '(') $leftCnt++;
            if ($comment[$i] == ')') $rightCnt++;
            if ($leftCnt == $rightCnt && $leftCnt != 0) {
                $lastPos = $i;
                break;
            }
        }
        return substr($comment, stripos($comment, '(', $firstPos) + 1, $lastPos - stripos($comment, '(', $firstPos) - 1);
    }
    

}