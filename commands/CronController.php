<?php
namespace app\commands;

use yii\console\Controller;
use yii2tech\crontab\CronTab;

/**
 * Class CronController
 * @package app\commands
 * первоначальные настройки заданий cron 
 * запуск php yii cron
 */
class CronController extends Controller
{
    public function actionIndex()
    {
        $crontab = new CronTab();
        $crontab->removeAll();
        $crontab->headLines = [
            'SHELL=/bin/bash',
            'MAILTO=root'
        ];
        $crontab->setJobs([
            //TODO add jobs
        ]);
        $crontab->apply();
    }
}