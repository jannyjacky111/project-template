<?php

return [
    'POST v1/register' => 'v1/apitemplate-auth/register',
    'POST v1/auth' => 'v1/apitemplate-auth/auth',
    'GET v1/all' => 'v1/apitemplate/all',
    'GET v1/me' => 'v1/apitemplate/me',
    'POST v1/me/avatar' => 'v1/apitemplate/upload-avatar',
    'POST v1/device-token' => 'v1/apitemplate/check-device-token'
];

?>