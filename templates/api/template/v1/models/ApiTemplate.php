<?php

namespace app\api\apitemplate\v1\models;


use app\admin\models\Admapitemplate;
use app\tables\TblDevices;
use yii\helpers\Url;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * Class apitemplate
 * @package app\api\manager\v1\models
 */

class ApiTemplate implements IdentityInterface
{

    const SCENARIO_VIEWTOKEN = 'view token';

    public function fields()
    {
        $mainFields = [
            'id',
            'avatar' => function () {
                return ($this->file) ? Url::to($this->file->filename, true) : null;
            },
            'firstname',
            'surname',
            'email',
            'phone_number'
        ];

        $dopFields = [];

        if ($this->scenario == self::SCENARIO_VIEWTOKEN) {
            $dopFields[] = 'token';
        }

        return ArrayHelper::merge($mainFields, $dopFields);

    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * находит модель активного профиля клиента (выполняемого в данных момент) по id клиента
     * @return Profile
     */
    public function getCurrentProfile()
    {
        return Profile::find()->where(['apitemplate_id' => $this->id])->andWhere([
            'or',
            ['>=', 'finish_dt', date('Y-m-d', time())],
            ['finish_dt' =>  null]
        ])->one();
    }


    /**
     * возвращает true, если клиент может создать анкету , иначе false
     * клиент может создать анкету , если у него нет анкет или время окончания последней анкеты меньше текущей даты или еще не заполнено
     * @return bool
     */
    public function canCreateProfile()
    {
        return !Profile::find()->where(['apitemplate_id' => $this->id])->andWhere([
            'or',
            ['>=', 'finish_dt', date('Y-m-d', time())],
            ['finish_dt' =>  null]
        ])->count();
    }

    /**
     * проверяем устройство , с которого авторизовался пользователь
     * если оно еще не сохранено в таблице TblDevices, сохраняем
     * @param $post
     * @return bool
     */
   public function checkDevice($post)
   {
       if (array_diff(['device_id', 'device_token', 'device_platform'], array_keys($post))) return false;
       
       $device = TblDevices::findOne(['device_id' => $post['device_id'], 'apitemplate_id' => $this->id]);
       //если у клиента нет устройства с таким id, сохраняем
       if (!$device) {
           $device = new TblDevices([
               'device_id' => $post['device_id'],
               'device_token' => $post['device_token'],
               'device_platform' => '['.strtolower($post['device_platform']).']',
               'apitemplate_id' => $this->id
           ]);
       //если токен устройства изменился, сохраняем новый
       } else if ($device->device_token != $post['device_token']) $device->device_token = $post['device_token'];
       else return true;
       
       return $device->save();
   }

    /**
     * @throws \Exception
     * @throws \Throwable
     * удаление всех устройств клиента
     */
    public function deleteDevice()
    {
        foreach ($this->tblDevices as $device) {
            $device->delete();
        }
    }

    /**
     * @return array
     * возвращает информацию для раздела "полезное" клиента
     */
    public function getRecommendations()
    {
        $profile = ApiTemplate::findOne(\Yii::$app->user->id)->getCurrentProfile();
        if (!is_null($profile)) $currentProfileWeek = $profile->getCurrentWeek();
        else $currentProfileWeek = null;

        if ($profile && $profile->group_id && $profile->program) {
            if ($currentProfileWeek) {
                if ($currentProfileWeek->special_case) {
                    $weekData['meal_plan'] = $weekData['recommendation'] = AdmSetting::getValue('special_case_message');
                } else $weekData = json_decode($currentProfileWeek->tng_program_json, true);
            } else {
                $profileWeek = ProfileWeek::find()->where(['profile_id' => $profile->id, 'number_week' => 1])->joinWith('week')->one();
                if ($profileWeek && $profileWeek->start_dt > date('Y-m-d', time())) {
                    $weekData['meal_plan'] = $weekData['recommendation'] = 'Ваша программа начнется '.\Yii::$app->formatter->asDate($profileWeek->start_dt);
                } else $weekData['meal_plan'] = $weekData['recommendation'] = AdmSetting::getValue('report_forget_message');
            }
        } else $weekData['meal_plan'] = $weekData['recommendation'] = null;


        $recommendations = [
            'meal_plan' => $weekData['meal_plan'],
            'personal' => $profile && ($profile->program_id || $profile->group_id)  ? ([
                'curator_recommendations' => Recommendation::findAll(['profile_id' => $profile->id]),
                'new_recommendations' => (int)Recommendation::find()->where([
                    'profile_id' => $profile->id,
                    'is_new' => 1
                ])->count(),
                'week_recommendations' => $weekData['recommendation'],
                'common_recommendations' => $profile->program ? $profile->program->recommendation : null
            ]) : null,
            'articles' => Article::find()->all()
        ];
        
        return $recommendations;
    }

    /**
     * @return static[]
     * возвращает чаты клиента
     */
    public function getChats()
    {
        return Chatroom::findAll([
            'is_active' => 1,
            'user_id' => $this->id
        ]);
    }

}