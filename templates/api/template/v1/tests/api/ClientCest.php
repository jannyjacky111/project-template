<?php

/**
 * Class apitemplateCest
 * class for api test apitemplate auth
 *
 * ЗАПУСК ТЕСТОВ:
 * находясь в app/api/apitemplate/v1
 * vendor/bin/codecept run - все тесты
 * vendor/bin/codecept run api - только api тесты
 * vendor/bin/codecept run api apitemplateCest - только тесты этого файла
 * vendor\bin\codecept run api - если вы все еще пользуетесь Windows
 */


/**
 *
 *
 * @SWG\Post(
 *     path="/auth",
 *     summary="/auth",
 *     description="Returns a current apitemplate fields with token",
 *     operationId="apitemplateAuth",
 *     tags={"auth"},
 *     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         description="request parameters in body",
 *         in="body",
 *         name="id",
 *         required=true,
 *         type="integer",
 *         @SWG\Schema($ref="#/definitions/PostapitemplateParams")
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *         @SWG\Schema($ref="#/definitions/apitemplate")
 *     ),
 *     @SWG\Response(
 *         response=400,
 *         description="Bad Request",
 *         @SWG\Schema($ref="#/definitions/Error")
 *     )
 * )
 *
 *
 * @SWG\Definition(
 *     name="PostapitemplateParams",
 *     type="object",
 *     required={"email","password"},
 *     @SWG\Property(
 *          name="email",
 *          type="string",
 *          title="apitemplate email",
 *          example="morozof@mail.com"
 *     ),
 *     @SWG\Property(
 *          name="password",
 *          type="integer",
 *          title="apitemplate password",
 *          example="yj8oyi8wv"
 *     )
 * )
 *
 *
 * @SWG\Definition(
 *     name="Error",
 *     type="object",
 *     @SWG\Property(
 *          name="name",
 *          type="string"
 *     ),
 *     @SWG\Property(
 *          name="message",
 *          type="string"
 *     ),
 *     @SWG\Property(
 *          name="code",
 *          type="integer"
 *     ),
 *     @SWG\Property(
 *          name="status",
 *          type="integer"
 *     ),
 *     @SWG\Property(
 *          name="type",
 *          type="string"
 *     )
 * )
 *
 * @SWG\Security(
 *     APIKeyHeader="[]"
 * )
 *
 * @SWG\SecurityDefinition(
 *     title="APIKeyHeader",
 *     type="apiKey",
 *     description="Field format 'Bearer {token}'",
 *     name="Authorization",
 *     in="header"
 * )
 *
 * @SWG\Get(
 *     path="/all",
 *     summary="/all",
 *     description="Returns full apitemplate's data",
 *     operationId="all",
 *     tags={"me"},
 *     produces={"application/json"},
 *     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *         @SWG\Schema($ref="#/definitions/AllapitemplateInfo")
 *     ),
 *     @SWG\Response(
 *         response="401",
 *         description="Unauthorized",
 *         @SWG\Schema($ref="#/definitions/Error")
 *     )
 * )
 *
 *
 *
 *
 *
 *  @SWG\Definition(
 *     name="AllapitemplateInfo",
 *     type="object",
 *     @SWG\Property(
 *          name="me",
 *          type="object",
 *          $ref="#/definitions/apitemplate"
 *     ),
 *     @SWG\Property(
 *          name="profile",
 *          type="object",
 *          $ref="#/definitions/Profile"
 *     ),
 *     @SWG\Property(
 *          name="training-program",
 *          type="array",
 *          @SWG\Items($ref="#/definitions/ProfileWeeks")
 *     ),
 *     @SWG\Property(
 *          name="recommendations",
 *          type="object",
 *          $ref="#/definitions/AllRecommendations"
 *     ),
 *     @SWG\Property(
 *          name="chats",
 *          type="array",
 *          @SWG\Items($ref="#/definitions/Chat")
 *     ),
 *     @SWG\Property(
 *          name="reviews",
 *          type="array",
 *          @SWG\Items($ref="#/definitions/Review")
 *     ),
 *     @SWG\Property(
 *          name="feedbacks",
 *          type="array",
 *          @SWG\Items($ref="#/definitions/PublicReview")
 *     ),
 *     @SWG\Property(
 *          name="catalog",
 *          type="object",
 *          $ref="#/definitions/AllValuesCatalog"
 *     )
 * )
 */


/**
*
*  @SWG\Definition(
*     name="apitemplate",
*     type="object",
*     required={"id","avatar","firstname","surname", "phone_number", "email", "token"},
*     @SWG\Property(
*          name="id",
*          type="integer"
*     ),
*     @SWG\Property(
*          name="avatar",
*          type="string"
*     ),
*     @SWG\Property(
*          name="firstname",
*          type="string"
*     ),
*     @SWG\Property(
*          name="surname",
*          type="string"
*     ),
*     @SWG\Property(
*          name="email",
*          type="string"
*     ),
*     @SWG\Property(
*          name="phone_number",
*          type="string"
*     ),
*     @SWG\Property(
*          name="token",
*          type="string"
*     )
* )
*
*/

/**
*   @SWG\Get(
*     path="/me",
*     summary="/me",
*     description="Returns a current apitemplate fields",
*     operationId="me",
*     tags={"me"},
*     produces={"application/xml", "application/json"},
*     @SWG\Response(
*         response=200,
*         description="successful operation",
*         @SWG\Schema($ref="#/definitions/apitemplateWithOutToken")
*     ),
*     @SWG\Response(
*         response="404",
*         description="Not Found",
*         @SWG\Schema($ref="#/definitions/Error")
*     ),
*     @SWG\Response(
*         response="401",
*         description="Unauthorized",
*         @SWG\Schema($ref="#/definitions/Error")
*     )
* )
*/


/**
*  @SWG\Definition(
*     name="apitemplateWithOutToken",
*     type="object",
*     required={"id","avatar","firstname","surname", "phone_number", "email"},
*     @SWG\Property(
*          name="id",
*          type="integer"
*     ),
*     @SWG\Property(
*          name="avatar",
*          type="string"
*     ),
*     @SWG\Property(
*          name="firstname",
*          type="string"
*     ),
*     @SWG\Property(
*          name="surname",
*          type="string"
*     ),
*     @SWG\Property(
*          name="email",
*          type="string"
*     ),
*     @SWG\Property(
*          name="phone_number",
*          type="string"
*     )
* )
*
*
*   @SWG\Post(
*     path="/me/avatar",
*     summary="/me/avatar",
*     description="Returns a current apitemplate fields",
*     operationId="me",
*     tags={"me"},
*     produces={"application/xml", "application/json"},
*     @SWG\Parameter(
*         description="apitemplate avatar",
*         in="formData",
*         name="avatar",
*         type="file"
*    ),
*     @SWG\Response(
*         response=200,
*         description="successful operation",
*         @SWG\Schema($ref="#/definitions/apitemplateWithOutToken")
*     ),
*     @SWG\Response(
*         response="401",
*         description="Unauthorized",
*         @SWG\Schema($ref="#/definitions/Error")
*     ),
*     @SWG\Response(
*         response=400,
*         description="Bad Request",
*         @SWG\Schema($ref="#/definitions/Error")
*     )
* )
*
*
*/

/**
*
*   @SWG\Post(
*     path="/logout",
*     summary="/logout",
*     description="Returns empty array",
*     operationId="apitemplateLogout",
*     tags={"me"},
*     produces={"application/xml", "application/json"},
*     @SWG\Response(
*         response=200,
*         description="successful operation",
*     ),
*     @SWG\Response(
*         response="404",
*         description="Not Found",
*         @SWG\Schema($ref="#/definitions/Error")
*     ),
*     @SWG\Response(
*         response="401",
*         description="Unauthorized",
*         @SWG\Schema($ref="#/definitions/Error")
*     )
* )
*
*/

/**
*
* @SWG\Post(
*     path="/device-token",
*     summary="/device-token",
*     description="Rewrite device token",
*     operationId="DeviceToken",
*     tags={"me"},
*     produces={"application/xml", "application/json"},
 *     @SWG\Parameter(
 *         description="device id",
 *         in="body",
 *         name="device_id",
 *         @SWG\Schema(
 *              type="string"
 *          )
 *    ),
 *     @SWG\Parameter(
 *         description="device token",
 *         in="body",
 *         name="device_token",
 *         @SWG\Schema(
 *              type="string"
 *          )
 *    ),
*     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *         @SWG\Schema(
 *              type="boolean"
 *          )
*     ),
*     @SWG\Response(
*         response="400",
*         description="Bad Request",
*         @SWG\Schema($ref="#/definitions/Error")
*     ),
*     @SWG\Response(
*         response="401",
*         description="Unauthorized",
*         @SWG\Schema($ref="#/definitions/Error")
*     )
* )
*
*/
use yii\helpers\Json;

class apitemplateCest
{
    public $token;
    public $EmailAuth = 'sample@email.com';
    public $password = '37069';


    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
    }

    public function _after(ApiTester $I)
    {

    }

    //пробуем авторизоваться
    public function tryAuth(ApiTester $I)
    {
        $I->sendPOST('/auth', ['email' => $this->EmailAuth, 'password' => $this->password]);
        $response = Json::decode($I->grabResponse());
        $this->token = $response['token'];
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'avatar' => 'string|null',
            'firstname' => 'string',
            'surname' => 'string',
            'email' => 'string',
            'phone_number' => 'string',
            'token' => 'string',
        ]);
    }

    //пробуем выйти из системы
    public function tryLogout(ApiTester $I)
    {
        $I->haveHttpHeader('Authorization', 'Bearer ' . $this->token);
        $this->token = '';
        $I->sendPOST('/logout');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    //выход их системы без токена

    public function tryLogoutWithoutToken(ApiTester $I)
    {
        $I->sendPOST('/logout');
        $I->seeResponseCodeIs(401);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Unauthorized',
            'message' => 'Your request was made with invalid credentials.',
            'code' => '0',
            'status' => 401,
            'type' => "yii\\web\\UnauthorizedHttpException"
        ]);
    }

    //авторизоваться с неверным паролем
    public function tryAuthWrongPassword(ApiTester $I)
    {
        $I->sendPOST('/auth', ['email' => $this->EmailAuth, 'password' => "wrong-password"]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Bad Request',
            'message' => 'Wrong password.',
            'code' => '0',
            'status' => 400,
            'type' => "yii\\web\\BadRequestHttpException"
        ]);
    }


    //авторизоваться с несуществующим менеджером
    public function tryAuthWrongEmail(ApiTester $I)
    {
        $I->sendPOST('/auth', ['email' => 'wrong@email.com', 'password' => $this->password]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Bad Request',
            'message' => 'apitemplate not found.',
            'code' => '0',
            'status' => 400,
            'type' => "yii\\web\\BadRequestHttpException"
        ]);
    }

    //авторизоваться без необходимых параметров
    public function tryAuthWithoutEmail(ApiTester $I)
    {
        $I->sendPOST('/auth', ['email' => '', 'password' => $this->password]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Bad Request',
            'message' => 'Email is not set.',
            'code' => '0',
            'status' => 400,
            'type' => "yii\\web\\BadRequestHttpException"
        ]);
    }

    public function tryAuthWithoutPassword(ApiTester $I)
    {
        $I->sendPOST('/auth', ['email' => $this->EmailAuth, 'password' => '']);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Bad Request',
            'message' => 'Password is not set.',
            'code' => '0',
            'status' => 400,
            'type' => "yii\\web\\BadRequestHttpException"
        ]);
    }

    //убедимся что API закрыто

    public function tryAuthByGet(ApiTester $I)
    {
        $I->sendGET('/auth');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryAuthByPut(ApiTester $I)
    {
        $I->sendPUT('/auth');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryAuthByPatch(ApiTester $I)
    {
        $I->sendPATCH('/auth');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryAuthByDelete(ApiTester $I)
    {
        $I->sendDELETE('/auth');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    // /logout

    public function tryLogoutPost(ApiTester $I)
    {
        $I->sendGET('/logout');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryLogoutPut(ApiTester $I)
    {
        $I->sendPUT('/logout');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryLogoutPatch(ApiTester $I)
    {
        $I->sendPATCH('/logout');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }

    public function tryLogoutDelete(ApiTester $I)
    {
        $I->sendDELETE('/logout');
        $I->seeResponseCodeIs(404);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'name' => 'Not Found',
            'message' => 'Page not found.',
            'code' => '0',
            'status' => 404,
            'type' => "yii\\web\\NotFoundHttpException"
        ]);
    }
}