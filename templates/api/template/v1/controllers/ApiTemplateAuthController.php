<?php

namespace app\api\apitemplate\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use app\api\apitemplate\v1\models\ApiTemplate;
use yii\web\ServerErrorHttpException;
use app\admin\models\AdmEmailBuffer;
use app\api\components\Helper;


class ApiTemplateAuthController extends Controller
{
    //авторизация
    public function actionAuth()
    {
        $request = Yii::$app->request;

        //проверяем на наличие необходимых переменных в запросе
        $fields = ['email', 'password'];
        
        //$fields = ['email', 'password', 'device_id', 'device_token'];
        foreach ($fields as $field) {
            if (!$request->post($field)) throw new BadRequestHttpException($field . \Yii::t('app', "[is not set.]"));
        }
        
        $email = $request->post('email');
        $code = $request->post('password');
        

        if ($apitemplate = ApiTemplate::find()->where(['email' => $email])->one()){

            if (Yii::$app->getSecurity()->validatePassword($code , $apitemplate->password_hash)){

                //устанавливаем токен
                $apitemplate->setToken();

                $apitemplate->scenario = ApiTemplate::SCENARIO_VIEWTOKEN;

                // проверяем сохранено ли устройство входа
                // $apitemplate->checkDevice($request->post());

                return $apitemplate;

            } else {

                throw new BadRequestHttpException(\Yii::t('app/api', "[Wrong password.]"));
            }

        } else {

            throw new BadRequestHttpException(\Yii::t('app/api', "[ApiTemplate not found.]"));
        }
    }


    // восстановление пароля
    public function actionPasswordRestore () {
        //получаем body запроса
        $data = Yii::$app->getRequest()->getBodyParams();


        if( !($email = $data['email'])) {
            throw new BadRequestHttpException(\Yii::t('app/api', "[Email is not set.]"));
        }

        //проверка, существует ли email
        if( !($model = ApiTemplate::findOne(['email' => $email])) ) {
            throw new BadRequestHttpException(\Yii::t('app/api', "[User not found.]"));
        }

        $code = Yii::$app->getSecurity()->generateRandomString(6);

        $model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($code);

        if($model->validate()){

            $mail = Yii::$app->mailer->compose('@app/mail/password', [
                'name' => $model->firstname,
                'username' => $model->email,
                'password' => $code
            ])
                ->setTo($email)
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject('Восстановление пароля');

            if ( $mail->send()) {
                if ($model->save()) {
                    return [];
                }
            } else {
                throw new ServerErrorHttpException(\Yii::t('app', "[Mail not send.]"));
            }

        }else {
            throw new BadRequestHttpException(\Yii::t('app', "[Email is Invalid.]"));
        }
    }


    //регистрация клиента

    public function actionRegister () {

        $data = Yii::$app->getRequest()->getBodyParams();

        if ( empty($data['password']) || $data['password'] !== $data['password_confirm'] ) {
            throw new BadRequestHttpException(\Yii::t('app', "[Wrong password.]"));
        }

        $model =  new ApiTemplate();

        $model->setAttributes([
            'firstname' => $data['firstname'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash($data['password']),
            'phone_number' => $data['phone_number']
        ]);

        if($model->validate()) {

            if($model->save()) {

                    $mail = Yii::$app->mailer->compose('@app/mail/hello',[
                        'model' => $model,
                        'password' => $data['password']
                    ])
                    ->setTo($data['email'])
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject('Регистрация');

                if( !($mail->send())) {
                    throw new ServerErrorHttpException(\Yii::t('app', "[Mail not send.]"));
                }

                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);

                return $model;

            }else {
                throw new ServerErrorHttpException(\Yii::t('app', "[ApiTemplate not created.]"));
            }

        }else {
            throw new BadRequestHttpException(Helper::errorsToStr($model->errors));
        }

    }

}