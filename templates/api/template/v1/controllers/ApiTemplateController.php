<?php

namespace app\api\apitemplate\v1\controllers;

use Yii;
use app\api\components\Helper;
use Codeception\Module\Cli;
use app\api\MainApiController;
use app\api\apitemplate\v1\models\apitemplate;
use app\admin\models\AdmEmailBuffer;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;


/**
 * Class apitemplateController
 * @package app\api\apitemplate\v1\controllers
 *
 */
class ApiTemplateController extends MainApiController
{
    /**
     * возвращает информацию о себе клиенту,
     * если клиент существует и авторизован
     * @return static
     * @throws NotFoundHttpException
     */
    public function actionMe()
    {
        $model = ApiTemplate::findOne(['id' => Yii::$app->user->id]);
        if (!empty($model)) return $model;
        else throw new NotFoundHttpException();
    }

    public function actionLogout ()
    {
        if ($me = ApiTemplate::findOne(Yii::$app->user->id)) {

            $me->deleteToken();
//            $me->deleteDevice();
            return [];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionEmailreset ()
    {
        $userId = Yii::$app->user->id;

        //получаем body запроса
        $data = Yii::$app->getRequest()->getBodyParams();

        if( !($email = $data['email'])) {
            throw new BadRequestHttpException(\Yii::t('app', "[Email is not set.]"));
        }

        //проверка, существует ли email у других пользователей
        if( ApiTemplate::find()->where(['<>', 'id', $userId])->andWhere(['email' => $email])->one() ) {
            throw new BadRequestHttpException(\Yii::t('app', "[This Email is exist.]"));
        }

        //перед записью удаляем все записи с таким email
        AdmEmailBuffer::deleteAll(['email' => $email]);

        $emailBuffer = new AdmEmailBuffer();

        $response = Yii::$app->getResponse();

        $code = Yii::$app->getSecurity()->generateRandomString(6);

        $emailBuffer->setAttributes([
            'user_id' => $userId,
            'email' => $email,
            'code_hash' => Yii::$app->getSecurity()->generatePasswordHash($code),
            'code_expire' => Yii::$app->formatter->asDate('+ 60 min', 'php:Y-m-d h:i:s'),
        ]);

        if($emailBuffer->validate()){

            $mail = Yii::$app->mailer->compose()
                ->setTo($email)
                // TODO вывести в конфиг email "from"
                ->setFrom(['info@viafit.com' => 'Виафит'])
                ->setSubject('Восстановление пароля')
                ->setTextBody('Ваш новый пароль: ' . $code);

            if ( $mail->send()) {
                if ($emailBuffer->save()) {

                    $response->setStatusCode(201);
                    return [];
                }
            } else {

                throw new ServerErrorHttpException(\Yii::t('app', "[Mail not send.]"));

            }

        }else {

            throw new BadRequestHttpException(\Yii::t('app', "[Email is Invalid.]"));
        }
    }

    //валидация кода, подтверждающего смену пароль
    public function actionEmailcodevalidate ()
    {

        $apitemplateId = Yii::$app->user->id;

        //получаем body запроса
        $data = Yii::$app->getRequest()->getBodyParams();

        //проверка, передан ли email
        if( !($data['email'])) {

            throw new BadRequestHttpException(\Yii::t('app/api', "[Email is not set.]"));
        }

        //проверка, передан ли code
        if( !($data['code'])) {

            throw new BadRequestHttpException(\Yii::t('app/api', "[Code is not set.]"));
        }

        //проверяем, есть ли в базе-буффере такой email
        if( !( $emailBuffer = AdmEmailBuffer::find()->where(['email' => $data['email'], 'apitemplate_id' => $apitemplateId])->one())) {

            throw new NotFoundHttpException(\Yii::t('app/api', "[Email not found]"));
        }

        //проверяем актуальность кода по дате
        if ($emailBuffer->code_expire < Yii::$app->formatter->asDate('now', 'php:Y-m-d h:i:s')){

            throw new BadRequestHttpException(\Yii::t('app/api', "[Code not found.]"));
        }

        if (Yii::$app->getSecurity()->validatePassword($data['code'], $emailBuffer->code_hash)) {

            $response = Yii::$app->getResponse();

            $model = ApiTemplate::findOne($apitemplateId);

            $model->email = $emailBuffer->email;

            if ($model->validate() && $model->save()) {

                $response->setStatusCode(200);
                return [];

            } else {

                throw new ServerErrorHttpException(\Yii::t('app/api', "[apitemplate not save]"));
            }

        } else {
            throw new BadRequestHttpException(\Yii::t('app/api', "[Email not found.]"));
        }
    }


    //загрузка аватара

    public function actionUploadAvatar () {

        $image = new AdmFile();

        if( ($image->image = UploadedFile::getInstanceByName('avatar'))) {

            if($image->imageUpload()) {
                $me = ApiTemplate::findOne(Yii::$app->user->id);
                $oldAvatarId = $me->avatar_id;
                $me->avatar_id = $image->id;
            } else {
                throw new BadRequestHttpException(Helper::errorsToStr($image->errors));
            }

            if( !($me->save())) {
                throw new ServerErrorHttpException(\Yii::t('app', "[apitemplate not saved]"));
            }

            if(!empty($oldAvatarId)) {
                AdmFile::findOne($oldAvatarId)->delete();
            }

            return $me;
        }else {
            throw new BadRequestHttpException(\Yii::t('app', "[File not found.]"));
        }
    }

}