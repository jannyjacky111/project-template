<?php
namespace app\admin\models;

use app\tables\TblImgBuffer;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;

/**
 * Class AdmImgBuffer
 * @package app\admin\models
 */
class AdmImgBuffer extends TblImgBuffer
{
    /**
     * @var UploadedFile
     */
    public $image;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['image'], 'file', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 5, 'minSize' => 1024 * 2, 'mimeTypes' => ['image/*']],
            ]);
    }

    /**
     * @param $name
     * @param $path
     * @return int
     * сохраняет изображение по имени
     * возвращает id во временной таблице при успехе
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     */
    public static function saveImageByName($name, $path)
    {
        $model = new self;
        $model->image = UploadedFile::getInstanceByName($name);
        
        if ($model->validate(['image'])) return self::saveFile($model->image, $path);
        else throw new BadRequestHttpException(implode(', ', $model->errors['image']));
        
    }

    /**
     * @param $file
     * @param string $path
     *
     * функция сохраняет изображение, превью и информацию об изображении
     * возвращает id модели при успешном сохранении или ошибку
     * @return int
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     */
    public static function saveFile($file, $path = 'uploads', $size = [], $previewSize = [])
    {
        if(!is_null($file)) {
            $info['mime'] = $file->type;
            
            $fileName = AdmFile::saveImageFile($file, $path, $size);
            $previewName = AdmFile::saveImageFile($file, $path.'/previews', $previewSize);
            if (empty($fileName) || empty($previewName)) {
                throw new ServerErrorHttpException("Image didn't save");
            }
            $info['filename'] = '/'.$path.'/' . $fileName;
            $info['preview'] = '/'.$path.'/previews/' . $previewName;
            return self::saveInfo($info);
        } else throw new BadRequestHttpException('Image is empty');
    }


    /**
     * @param $info
     * @return int
     *
     * функция сохраняет информацию об изображении в таблице TblImgBuffer
     *
     * возвращает id модели при успешном сохранении, иначе null
     * @return boolean
     * @throws BadRequestHttpException
     */
    public function saveInfo($info)
    {
        $model = new self;
        $info = ['AdmImgBuffer' => $info];
        if ($model->load($info) && $model->save()) return $model->id;
        else throw new BadRequestHttpException(implode(', ', $model->errors));
    }
}