<?php

namespace app\admin\models;

use app\tables\TblEmailBuffer;
use yii\helpers\ArrayHelper;

class AdmEmailBuffer extends TblEmailBuffer
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                ['email', 'email'],
            ]);
    }

}