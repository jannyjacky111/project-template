<?php

namespace app\admin\models;

use Yii;
use Imagine\Image\Box;
use app\tables\TblFiles;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

class AdmFile extends TblFiles
{
    /**
     * @var UploadedFile
     */
    public $image;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [
                [['image'], 'file', 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 5, 'minSize' => 1024 * 2, 'mimeTypes' => ['image/*']],
            ]);
    }

    public function imageUpload($imgWidth = 600, $imgHeight = 600)
    {
        $filename = uniqid() . '.' . $this->image->extension;

        //$preview = 'tmb_'. $filename;

        $this->filename = '/admin/images/' . $filename;
        $this->preview = '/admin/images/' . $filename;
        $this->mime = FileHelper::getMimeTypeByExtension($filename);

        if ($this->validate()) {
            //обрезаем изображение
            $tmb = Image::thumbnail($this->image->tempName, $imgWidth, $imgHeight);
            //если получилось сохранить изображение - сохраняем модель
            if($tmb->save(Yii::getAlias('@app').'/web/admin/images/' . $filename, ['quality' => 80])){
                $this->save();
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
    
    /**
     * @param $name
     * @param $path
     * @param $size
     * @param $previewSize
     * функция сохраняет все файлы из инпута с имененм $name
     * и записывает данные о них в соответствующую таблицу
     *
     * возвращает массив id всех сохраненных файлов
     * @return array
     */
    public static function saveFiles($name, $path = 'uploads', $size = [], $previewSize = [])
    {
        $files = UploadedFile::getInstancesByName($name);
        if (empty($files)) return [];
        $resultFileModels = [];
        foreach ($files as $file) {
            $resultFileModels[] = self::saveImage($file, $path, $size, $previewSize);
        }
        return $resultFileModels;
    }

    /**
     * @param $file
     * @param $path
     * @param $size
     * @param $previewSize
     * функция сохраняет изображение, превью и информацию об изображении блога
     * возвращает id модели при успешном сохранении, иначе null
     * @return boolean
     */
    public static function saveImage($file, $path = 'uploads', $size = [], $previewSize = [])
    {
        if(!is_null($file)) {
            $info['mime'] = $file->type;
            $fileName = self::saveImageFile($file, $path, $size);
            $previewName = self::saveImageFile($file, $path.'/previews', $previewSize);
            if (empty($fileName) || empty($previewName)) {
                Yii::$app->session->setFlash('danger', 'Не удалось сохранить файл');
                return null;
            }
            $info['filename'] = '/'.$path.'/' . $fileName;
            $info['preview'] = '/'.$path.'/previews/' . $previewName;
            return self::saveInfo($info);
        } else return null;
    }

    /**
     * @param $file
     * @param $path - путь для сохранения изображения, не обязателен
     * @param array $size - размер изображения, не обязателен
     *
     * функция сохраняет изображение в папке $path
     * возвращает имя файла при успешном сохранении
     * иначе пустую строку
     *
     * @return string
     */
    public static function saveImageFile($file, $path = 'uploads', $size = [])
    {
        if(!is_null($file)){
            $name_saved = uniqid().substr($file->name, strripos($file->name, '.'));
            $path = Yii::getAlias('@app').'/web/'.$path;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $image = Image::getImagine()->open($file->tempName);
            if (!empty($size)) $image->thumbnail(new Box($size[0], $size[1]))->save($path.'/'.$name_saved);
            else $image->save($path.'/'.$name_saved);
            return $name_saved;
        }
        return '';
    }

    /**
     * @param $file
     * @param $path - путь для сохранения файла, не обязателен
     *
     * функция сохраняет файл в папке $path
     * возвращает имя файла при успешном сохранении
     * иначе пустую строку
     *
     * @return string
     */
    public static function saveFile($file, $path = 'uploads')
    {
        if(!is_null($file)){
            $name_saved = uniqid().substr($file->name, strripos($file->name, '.'));
            $path = Yii::getAlias('@app').'/web/'.$path;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            file_put_contents($path.'/'.$name_saved, file_get_contents($file->tempName));
            return $name_saved;
        }
        return '';
    }

    /**
     * @param array $info
     *
     * функция сохраняет информацию об изображении в таблице TblBlogImages
     *
     * возвращает id модели при успешном сохранении, иначе null
     * @return boolean
     */
    public static function saveInfo($info)
    {
        $model = new self;
        $info = ['AdmFile' => $info];
        if ($model->load($info) && $model->save()) return $model->id;
        else return null;
    }

    /**
     * @return array
     * перевод лэйблов
     */
    public function attributeLabels()
    {
        $labels = [];
        foreach (parent::attributeLabels() as $attribute => $label)
        {
            $labels[$attribute] = \Yii::t('app', '['.$label.']');
        }
        return $labels;
    }

}