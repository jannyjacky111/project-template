<?php

namespace app\admin\models\forms;

use Yii;
use yii\base\Model;
use app\admin\models\AdmAdmin;

class RecoverPasswordForm extends Model
{
    public $email;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function sendPassword()
    {
        if ($this->validate()) {

            $admin = AdmAdmin::findByEmail($this->email);

            if($admin) {

                $new_pass = Yii::$app->security->generateRandomString(6);

                $admin->password_hash = Yii::$app->security->generatePasswordHash($new_pass);
                $admin->save(false, ['password_hash']);

                Yii::$app->mailer->compose('@app/mail/password', [
                    'name' => $admin->firstname,
                    'username' => $admin->username,
                    'password' => $new_pass
                ])->setTo($this->email)
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setSubject('Восстановление пароля')
                    ->send();

                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     * перевод лэйблов
     */
    public function attributeLabels()
    {
        $labels = [];
        foreach (parent::attributeLabels() as $attribute => $label)
        {
            $labels[$attribute] = \Yii::t('app/admin/models', '['.$label.']');
        }
        return $labels;
    }
}