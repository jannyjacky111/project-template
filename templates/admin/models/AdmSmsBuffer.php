<?php

namespace app\admin\models;


use app\tables\TblSmsBuffer;
use yii\helpers\ArrayHelper;

class AdmSmsBuffer extends TblSmsBuffer
{
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                ['phone_number', 'match', 'pattern' => '/7\d{10}$/'],
            ]
        );
    }
}