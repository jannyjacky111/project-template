<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="panel panel-sign">
    <div class="panel-title-sign mt-xl text-right">
        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i><?= Yii::t('app', '[Sign in]');?></h2>
    </div>
    <div class="panel-body">

    <?php $form = ActiveForm::begin([
        'id' => 'form-signin',
        'action' => '/admin/user/login'
    ]); ?>

        <?= $form->field($model, 'username', [
            'inputTemplate' => '<div class="input-group input-group-icon">
                                    {input}
                                    <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </span>
                                </div>',
            'inputOptions' => [
                'class' => 'form-control input-lg'
            ],
        ]) ?>

        <?= $form->field($model, 'password', [
            'template' => '<div class="clearfix">
                               {label}
                               <a href="/admin/user/password-recovery" class="pull-right">'.Yii::t('app', '[Lost password?]').'</a>
                           </div>
                           <div class="input-group input-group-icon">
                               {input}
                               <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-lock"></i>
                                    </span>
                               </span>
                           </div>
                           {error}',
            'inputOptions' => [
                'class' => 'form-control input-lg'
            ],
        ])->PasswordInput() ?>

        <div class="row">
            <div class="col-sm-8">
                <div class="checkbox-custom checkbox-default">
                    <input id="RememberMe" name="rememberme" type="checkbox"/>
                    <label for="RememberMe"><?= Yii::t('app/admin/models', '[Remember me]');?></label>
                </div>
            </div>
            <div class="col-sm-4 text-right">
                <?= Html::submitButton(Yii::t('app', '[Sign in]'), ['class' => 'btn btn-primary hidden-xs']) ?>
                <?= Html::submitButton(Yii::t('app', '[Sign in]'), ['class' => 'btn btn-primary btn-block btn-lg visible-xs mt-lg']) ?>
            </div>
        </div>

        <p class="text-center"></p>

    <?php ActiveForm::end() ?>
    </div>
</div>