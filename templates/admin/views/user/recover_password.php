<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>

<div class="panel panel-sign">
    <div class="panel-title-sign mt-xl text-right">
        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i><?= Yii::t('app', '[Recover password]');?></h2>
    </div>
    <div class="panel-body">

        <?php $flashes = Yii::$app->session->getAllFlashes();

        $flash_type = 'alert-info';
        $flash_message = Yii::t('app', '[Enter your e-mail below and we will send you reset instructions!]');

        if ($flashes) {
            foreach ($flashes as $type => $message) {
                $flash_type = $type;
                $flash_message = $message;
            }
        } ?>

        <?= Alert::widget([
            'options' => [
                'class' => $flash_type,
            ],
            'closeButton' => false,
            'body' => '<p class="m-none text-weight-semibold h6">' . $flash_message . '</p>',
        ]); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'form-password-recovery',
            'action' => 'password-recovery',
        ]); ?>

            <?php $submitButton = Html::submitButton(Yii::t('app', '[Reset!]'), ['class' => 'btn btn-primary btn-lg']); ?>

            <?= $form->field($model, 'email', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-btn">' . $submitButton . '</span></div>',
                'inputOptions' => [
                    'placeholder' => 'E-mail',
                    'class' => 'form-control input-lg'
                ],
            ])->label(false); ?>

            <p class="text-center mt-lg"><?= Yii::t('app', '[Remembered?]');?> <a href="/admin/user/login"><?= Yii::t('app', '[Sign in]');?></a>

        <?php ActiveForm::end() ?>
    </div>
</div>