<?php


/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\admin\assets\AppAsset;

$bundle = AppAsset::register($this);
$currentUser = Yii::$app->user->identity;
?>

<?php $this->beginPage() ?>
<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="<?= Yii::$app->charset ?>">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Head Libs -->
    <script src="<?=$bundle->baseUrl?>/assets/vendor/modernizr/modernizr.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
<section class="body">

    <!-- start: header -->
    <header class="header">

        <div class="logo-container">
            <a href="/admin" class="logo">
                <img src="<?=$bundle->logoUrl?>" height="70" style="margin-top: -18px;" alt="Porto Admin">
            </a>
            <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <!-- start: search & user box -->
        <div class="header-right">

            <span class="separator"></span>
            <?= \app\admin\components\AdmHeaderNotices::widget();?>
            <span class="separator"></span>

            <div id="userbox" class="userbox">
                <a href="#" data-toggle="dropdown">
                    <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                        <span class="name"><?= $currentUser->firstname . ' ' . $currentUser->surname ?></span>
                        <span class="role"><?= Yii::t('app', '[Administrator]');?></span>
                    </div>

                    <i class="fa custom-caret"></i>
                </a>

                <div class="dropdown-menu">
                    <ul class="list-unstyled">
                        <li class="divider"></li>
<!--                        <li>-->
<!--                            <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> --><?//= Yii::t('app', '[My profile]');?><!--</a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>-->
<!--                        </li>-->
                        <li>
                            <a role="menuitem" tabindex="-1" href="/admin/user/logout"><i class="fa fa-power-off"></i> <?= Yii::t('app', '[Logout]');?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
        <aside id="sidebar-left" class="sidebar-left">

            <div class="sidebar-header">
                <div class="sidebar-title">
                    Меню
                </div>
                <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>

            <div class="nano">
                <div class="nano-content">
                    <?=$this->render('_main-menu')?>

                    <hr class="separator" />
                </div>

            </div>

        </aside>
        <!-- end: sidebar -->
<!--        <div class="loading-overlay-showing">-->
            <section role="main" class="content-body">
                <header class="page-header">
                    <h2><?=$this->title?></h2>

                    <div class="right-wrapper pull-right mr-xl">
                        <?= Breadcrumbs::widget([
                            'homeLink' => ['label' => '<i class="fa fa-home"></i>', 'url' => Yii::$app->homeUrl],
                            'encodeLabels' => false,
                            'activeItemTemplate' => '<li class=\"active\"><span>{link}</span></li>\n',
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            'options' => [
                                'class' => 'breadcrumbs',
                            ],
                            'tag' => 'ol',
                        ]) ?>
                    </div>
                </header>
                <!-- start: page -->
                <div class="row">
                    <div class="col-md-12">

                        <?=$content?>

                    </div>
                </div>
                <!-- end: page -->
            </section>
<!--            <span class="loading-overlay" style="position: fixed; background: #ecedf080;margin-left: 300px;">-->
<!--                <span class="loader black"></span>-->
<!--            </span>-->
<!--        </div>-->

    </div>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
