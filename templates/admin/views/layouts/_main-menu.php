<?php

use app\admin\components\AdmNav;

?>
<nav id="menu" class="nav-main" role="navigation">
    <?= AdmNav::widget([
        'encodeLabels' => false,
        'activateParents' => true,
        'options' => [
            'class' => 'nav nav-main',
        ],
        'items' => [
            ['label' => '<i class="fa fa-home" aria-hidden="true"></i> <span>Главная</span>', 'url' => ['/site/index']],
            ['label' => '<i class="fa fa-home" aria-hidden="true"></i> <span>Контакты</span>', 'url' => ['/site/contact']],
            ['label' => '<i class="fa fa-home" aria-hidden="true"></i> <span>О нас</span>', 'url' => ['/site/about']]
        ],
    ]);?>
</nav>