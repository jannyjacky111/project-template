<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\admin\assets\AppAsset;

$bundle = AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="<?= Yii::$app->charset ?>">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
    <!-- start: page -->
    <section class="body-sign">
        <div class="center-sign">
            <?=$content?>
        </div>
    </section>
    <!-- end: page -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
