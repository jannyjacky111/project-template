$(document).ready(function(){
    $('.datepicker-input').datepicker({
        format: 'yyyy-mm-dd',
        orientation: 'bottom',
        language : 'ru',
        todayHighlight: true
    });
    $('.time-input').timepicker({
        minuteStep: 10,
        showMeridian: false,
        showSeconds: false,
        defaultTime: $(this).val()
    });
    $('.for-date').each(function () {
        var val = $(this).closest('.form-group').find('.hidden').val();
        $(this).val(val.substr(val.indexOf(' ') + 1));
        $(this).closest('.form-group').find('.datepicker-input').val(val.substr(0, val.indexOf(' ')));
    });
    $('.datepicker-input').change(function () {
        var time = $(this).closest('.form-group').find('.time-input').val();
        var date = $(this).val();
        $(this).closest('.form-group').find('.hidden').val(date + ' ' + time);
    });
    $('.time-input').change(function () {
        var time = $(this).val();
        var date = $(this).closest('.form-group').find('.datepicker-input').val();
        $(this).closest('.form-group').find('.hidden').val(date + ' ' + time);
    });
    $('select').select2({
        allowClear: true,
        placeholder: 'Не выбрано'
    });
    $('input[type="file"]').on('change', function(e){
        var container = $(this).closest('.fileupload');
        container.parent().find('img').remove();
        $.each(e.target.files, function(){
            var url = window.URL.createObjectURL(this);
            container.before('<img src="'+url+'" height="150">');
        });
    });
    $('#emoji-area').emojioneArea();
    $('.summernote').summernote({
        lang: 'ru-RU',
        height: 300,
        codemirror: {
            theme: 'monokai'
        },
        toolbar: [
            ['style', ['style']],
            ['insert', ['link']],
            ['misc', ['codeview']]
        ]
    });
    window.onbeforeunload = function(){
        $('.loading-overlay').parent().addClass('loading-overlay-showing');
    };

    $('.loading-overlay-showing').removeClass('loading-overlay-showing');
});
$(document).ajaxSuccess(function(){
    $('.datepicker-input').datepicker({
        format: 'yyyy-mm-dd',
        orientation: 'bottom',
        language : 'ru',
        todayHighlight: true
    });
    $('.time-input').timepicker({
        minuteStep: 10,
        showMeridian: false,
        showSeconds: false,
        defaultTime: $(this).val()
    });
    $('.for-date').each(function () {
        var val = $(this).closest('.form-group').find('.hidden').val();
        $(this).val(val.substr(val.indexOf(' ') + 1));
        $(this).closest('.form-group').find('.datepicker-input').val(val.substr(0, val.indexOf(' ')));
    });
    $('.datepicker-input').change(function () {
        var time = $(this).closest('.form-group').find('.time-input').val();
        var date = $(this).val();
        $(this).closest('.form-group').find('.hidden').val(date + ' ' + time);
    });
    $('.time-input').change(function () {
        var time = $(this).val();
        var date = $(this).closest('.form-group').find('.datepicker-input').val();
        $(this).closest('.form-group').find('.hidden').val(date + ' ' + time);
    });
    $('select').select2({
        allowClear: true,
        placeholder: 'Не выбрано'
    });
    $('input[type="file"]').on('change', function(e){
        var container = $(this).closest('.fileupload');
        container.parent().find('img').remove();
        $.each(e.target.files, function(){
            var url = window.URL.createObjectURL(this);
            container.before('<img src="'+url+'" height="150">');
        });
    });
    $('#emoji-area').emojioneArea();
    $('.summernote').summernote({
        lang: 'ru-RU',
        height: 150,
        codemirror: {
            theme: 'monokai'
        },
        toolbar: [
            ['style', ['style']],
            ['insert', ['link']],
            ['misc', ['codeview']]
        ]
    });
    window.onbeforeunload = function(){
        $('.loading-overlay').parent().addClass('loading-overlay-showing');
    };

    $('.loading-overlay-showing').removeClass('loading-overlay-showing');
});