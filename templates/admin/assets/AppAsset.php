<?php

namespace app\admin\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/admin/assets/theme';
    public $logoUrl = '/admin/admin-logo.png';
    
    public $css = [
        'css/site.css',
        /*<!-- Vendor CSS -->*/
        'assets/vendor/bootstrap/css/bootstrap.css',
        'assets/vendor/font-awesome/css/font-awesome.css',
        'assets/vendor/magnific-popup/magnific-popup.css',
        'assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css',
        'assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css',
        'assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css',
        'assets/vendor/select2/css/select2.css',
        'assets/vendor/select2-bootstrap-theme/select2-bootstrap.css',
        'assets/vendor/summernote/summernote.css',
        'assets/vendor/summernote/summernote-bs3.css',
        /*<!-- Theme CSS -->*/
        'assets/stylesheets/theme.css',

        /*<!-- Skin CSS -->*/
        'assets/stylesheets/skins/default.css',

        /*<!-- Theme Custom CSS -->*/
        'assets/stylesheets/theme-custom.css',

        'css/emojionearea.min.css',
        'css/site.css'
    ];

    public $js = [
        /*<!-- Vendor -->*/
        'assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js',
        'assets/vendor/nanoscroller/nanoscroller.js',
        'assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js',
        'assets/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.ru.min.js',
        'assets/vendor/magnific-popup/jquery.magnific-popup.js',
        'assets/vendor/jquery-placeholder/jquery-placeholder.js',
        'assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js',
        'assets/vendor/select2/js/select2.full.min.js',
        'assets/vendor/summernote/summernote.min.js',

        /*<!-- Theme Base, Components and Settings -->*/
        'assets/javascripts/theme.js',

        /*<!-- Theme Custom -->*/
        'assets/javascripts/theme.custom.js',

        /*<!-- Theme Initialization Files -->*/
        'assets/javascripts/theme.init.js',
        
        'js/emojionearea.min.js',
        'js/summernote-ru-RU.js',
        'js/admin.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
