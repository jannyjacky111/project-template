<?php

namespace app\admin\controllers;

use app\admin\models\forms\LoginForm;
use app\admin\models\forms\RecoverPasswordForm;
use Yii;

class UserController extends MainController
{

    public function actionLogin()
    {
        if(!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = ('login_layout');

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        return $this->render('login',[
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionPasswordRecovery()
    {
        if(!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = ('login_layout');

        $model = new RecoverPasswordForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendPassword()) {
                Yii::$app->session->setFlash('alert-info', Yii::t('app', '[We send new password to your e-mail]'));
                return $this->refresh();
            }else {
                Yii::$app->session->setFlash('alert-danger', Yii::t('app', '[User not found]'));
            }
        }

        return $this->render('recover_password', [
            'model' => $model,
        ]);
    }
}