<?php
/**
 * Created by PhpStorm.
 * User: jannyjacky
 * Date: 04.10.17
 * Time: 11:17
 */

namespace app\admin\components;

use app\admin\models\AdmMessage;
use app\admin\models\AdmProfile;
use app\admin\models\AdmReview;
use yii\bootstrap\Widget;

class AdmHeaderNotices extends Widget
{
    public $limit = 5;

    public function run()
    {
        return '';
        //сообщения клиентов в чате с администратором
        $messagesQuery = AdmMessage::find()->select(['chatroom_id', 'text', 'CONCAT(tbl_clients.firstname, " ", tbl_clients.surname) AS clientName', 'tbl_files.preview AS clientAvatar'])
            ->joinWith('chatroom')->joinWith('client')->leftJoin('tbl_files', 'tbl_clients.avatar_id = tbl_files.id')
            ->where(['tbl_chatrooms.curator_id' => null, 'is_new' => 1])->andWhere(['not', ['tbl_messages.user_id' => null]]);
        
        //неподтвержденные анкеты клиентов
        $profilesQuery = AdmProfile::find()->select(['city', 'age', 'CONCAT(tbl_clients.firstname, " ", tbl_clients.surname) AS clientName', 'tbl_files.preview AS clientAvatar'])
            ->joinWith('client')->leftJoin('tbl_files', 'tbl_clients.avatar_id = tbl_files.id')->where(['or', ['program_id' => null], ['group_id' => null]]);
        
//        //непроверенные отзывы
        $reviewsQuery = AdmReview::find()->select(['text', 'CONCAT(tbl_clients.firstname, " ", tbl_clients.surname) AS clientName', 'tbl_files.preview AS clientAvatar'])
            ->joinWith('profile')->leftJoin('tbl_clients', 'tbl_clients.id = tbl_profiles.client_id')->leftJoin('tbl_files', 'tbl_clients.avatar_id = tbl_files.id')->where(['status' => null]);
        
        return $this->render('notifications', [
            'messages' => $messagesQuery->orderBy(['tbl_messages.id' => SORT_DESC])->limit($this->limit)->all(),
            'newMessagesCount' => $messagesQuery->count(),
            'profiles' => $profilesQuery->orderBy(['tbl_profiles.id' => SORT_DESC])->limit($this->limit)->all(),
            'newProfilesCount' => $profilesQuery->count(),
            'reviews' => $reviewsQuery->orderBy(['tbl_reviews.id' => SORT_DESC])->limit($this->limit)->all(),
            'newReviewsCount' => $reviewsQuery->count(),
        ]);
    }
}