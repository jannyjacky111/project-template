<?php


namespace app\admin\components;

use yii\widgets\ActiveForm;

class AdmActiveForm extends ActiveForm
{
    /**
     * @var string the default field class name when calling [[field()]] to create a new field.
     * @see fieldConfig
     */
    public $fieldClass = 'yii\bootstrap\ActiveField';

    public $options = [
        'class' => 'form-horizontal form-bordered admin-form',
    ];

    public $fieldConfig = [
        'template' => "{label}\n<div class=\"col-md-6\">{input}\n{hint}\n{error}\n</div>",
        'labelOptions' => [
            'class' => 'col-md-3 control-label'
        ],
    ];

    /**
     * @var string the form layout. Either 'default', 'horizontal' or 'inline'.
     * By choosing a layout, an appropriate default field configuration is applied. This will
     * render the form fields with slightly different markup for each layout. You can
     * override these defaults through [[fieldConfig]].
     * @see \yii\bootstrap\ActiveField for details on Bootstrap 3 field configuration
     */
    public $layout = 'default';

}