<?php
namespace app\admin\components;

use Yii;
use yii\base\Widget;
use yii\bootstrap\Alert;

/**
 * Class AdmAlert
 * @package app\admin\components
 */
class AdmAlert extends Widget
{
    public function run()
    {
         foreach (Yii::$app->session->getAllFlashes() as $key => $message) :
            Alert::begin([
                'options' => ['class' => 'alert alert-'.$key]
            ]);
            echo $message;
            Alert::end();
        endforeach;
        parent::run();
    }
}
