<?php
use \yii\widgets\Pjax;
/**
 * Created by PhpStorm.
 * User: jannyjacky
 * Date: 04.10.17
 * Time: 11:16
 */
?>
<div class="notifications">
    <?php Pjax::begin(['id' => 'nots', 'timeout' => 10000]);?>
    <ul class="notifications">
        <li>
            <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                <i class="fa fa-commenting"></i>
                <?php if ($newReviewsCount) : ?><span class="badge"><?= $newReviewsCount;?></span><?php endif;?>
            </a>
            <?php if ($newReviewsCount) : ?>
                <div class="dropdown-menu notification-menu large">
                    <div class="notification-title">
                        <span class="pull-right label label-default"><?= $newReviewsCount;?></span>
                        <?= Yii::t('app', '[Reviews]');?>
                    </div>

                    <div class="content">
                        <ul>
                            <?php foreach ($reviews as $review) : ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to(['/reviews/view', 'id' => $review->id]);?>" class="clearfix" data-pjax="0">
                                        <figure class="image">
                                            <img src="<?= $review->clientAvatar;?>" class="img-circle" height="35" width="35">
                                        </figure>
                                        <span class="title"><?= $review->clientName;?></span>
                                        <span class="message"><?= $review->text;?></span>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                        <hr />

                        <div class="text-right">
                            <a href="<?= \yii\helpers\Url::to(['/reviews/index']);?>" class="view-more" data-pjax="0"><?= Yii::t('app', '[View All]');?></a>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </li>
        <li>
            <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                <i class="fa fa-envelope"></i>
                <?php if ($newMessagesCount) : ?><span class="badge"><?= $newMessagesCount;?></span><?php endif;?>
            </a>
            <?php if ($newMessagesCount) : ?>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="pull-right label label-default"><?= $newMessagesCount;?></span>
                        <?= Yii::t('app', '[Messages]');?>
                    </div>

                    <div class="content">
                        <ul>
                            <?php foreach ($messages as $message) : ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to(['/chatrooms/view', 'id' => $message->chatroom_id]);?>" class="clearfix" data-pjax="0">
                                        <figure class="image">
                                            <img src="<?= $message->clientAvatar;?>" class="img-circle" height="35" width="35">
                                        </figure>
                                        <span class="title"><?= $message->clientName;?></span>
                                        <span class="message"><?= $message->text;?></span>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>

                        <hr />

                        <div class="text-right">
                            <a href="<?= \yii\helpers\Url::to(['/chatrooms/index']);?>" class="view-more" data-pjax="0"><?= Yii::t('app', '[View All]');?></a>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </li>
        <li>
            <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                <i class="fa fa-file-text"></i>
                <?php if ($newProfilesCount) : ?> <span class="badge"><?= $newProfilesCount;?></span><?php endif;?>
            </a>
            <?php if ($newProfilesCount) : ?>
                <div class="dropdown-menu notification-menu">
                    <div class="notification-title">
                        <span class="pull-right label label-default"><?= $newProfilesCount;?></span>
                        <?= Yii::t('app', '[Profiles]');?>
                    </div>

                    <div class="content">
                        <ul>
                            <?php foreach($profiles as $profile) : ?>
                                <li>
                                    <a href="<?= \yii\helpers\Url::to(['/profiles/view', 'id' => $profile->id]);?>" class="clearfix" data-pjax="0">
                                        <figure class="image">
                                            <img src="<?= $profile->clientAvatar;?>" height="35" width="35" class="img-circle" />
                                        </figure>
                                        <span class="title"><?= $profile->clientName;?></span>
                                        <span class="message"><?= $profile->city.', '.$profile->age;?></span>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ul>

                        <hr />

                        <div class="text-right">
                            <a href="<?= \yii\helpers\Url::to(['/profiles/index']);?>" class="view-more" data-pjax="0"><?= Yii::t('app', '[View All]');?></a>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </li>
    </ul>
    <?php Pjax::end();?>
</div>

<?php

$script = <<< JS

$("document").ready(function(){
    
    $.pjax.defaults = false;
    
    setInterval(function () { $.pjax.reload({container:"#nots", url: "/admin/site/index"}); }, 30000);
    
});

JS;
$this->registerJs($script);
