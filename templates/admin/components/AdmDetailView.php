<?php

namespace app\admin\components;

use yii\widgets\DetailView;

class AdmDetailView extends DetailView
{
    public $options = ['class' => 'table table-striped mb-none admin-view-table'];
}