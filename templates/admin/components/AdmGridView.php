<?php

namespace app\admin\components;

use yii\grid\GridView;

class AdmGridView extends GridView
{
    public $tableOptions = ['class' => 'table table-striped mb-none admin-view-list'];
}