<?php

$config = require(__DIR__ . '/config-api.php');


//каждый модуль версия API
$config['modules'] = [
    'v1' => [
        'class' => 'app\api\apitemplate\v1\Module',
    ],
];

$config['bootstrap'][] = 'v1';

//у каждого API свой класс идентификации
$config['components']['user'] = [
    'identityClass' => 'app\api\apitemplate\v1\models\ApiTemplate'
];

return $config;