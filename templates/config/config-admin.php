<?php

$config = require(__DIR__ . '/config-common.php');

//дополняем ID приложения идентификатором
$config['id'] = $config['id'].' [admin]';

//
$config['components']['request'] = [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => 'pBSdxyPPF6lI4QtA3jCXxM8PcjbgbpD_',
    'baseUrl' => '/admin',
];

$config['layoutPath'] = '@app/admin/views/layouts';
$config['viewPath'] = '@app/admin/views';

//для консольного приложения свои настройки контроллеров
$config['controllerNamespace'] = 'app\admin\controllers';

//класс идентификации
$config['components']['user'] = [
    'identityClass' => 'app\admin\models\AdmAdmin',
    'identityCookie' => ['name' => '_admin_identity', 'httpOnly' => true],
    'enableAutoLogin' => true,
    'loginUrl' => ['/user/login']
];

$config['components']['session']['cookieParams']['path'] = '/admin';

$config['components']['assetManager'] = [
    'forceCopy' => false
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];

    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*', '::1'],
    ];
}

return $config;