<?php

namespace app\common\models;

use yii\httpclient\Client;
use yii\web\ServerErrorHttpException;
use Yii;
use yii\helpers\Json;

/**
 * Класс SMS. Реализует отправку смс.
 * @package app\models
 *
 * Если смс не отправлена выдаёт ошибку 500 с сообщением "Смс не отправлено".
 * Ответы ошибок от смс сервера можем куда-то собирать.
 */
class Sms
{
    /**
     * Функция отправки смс
     */
    public static function send($phoneNumber, $text)
    {
        //подключаем клиент для отправки смс
        $client = new Client();

        //API сервиса для отправки
        $url = 'https://api.bytehand.com/v1/send';

        //параметры GET запроса
        $params = array(
            'id' => Yii::$app->params['sms']['AccountId'],
            'key' => Yii::$app->params['sms']['AccountKey'],
            'to' => $phoneNumber,
            'text' => $text,
            'from' => Yii::$app->params['sms']['CompanyName'],
        );

        $url = $url . '?' . http_build_query($params);

        //ответ
        $response = $client->createRequest()
            ->setMethod('get')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($url)
            ->send();

        if ($response->isOk) {
            //{"status":0,"description":"205939340082042979"}
            //в случае успеха возвращаем идентификатор сообщения
            return Json::decode($response->content, false)->description;
        } else {
            //var_dump($response->content);
            throw new ServerErrorHttpException('SMS not sent.');
        }
    }

    /**
     *  Имитация отправки смс записывает в логи
     */

    public static function sendInLog($phoneNumber, $text)
    {
        Yii::info('Телефон:' . $phoneNumber . ' ' . 'Пинкод:' . $text, 'test_category');
        return true;
    }

    /**
     *  Функция проверки статуса смс
     */
    public static function statusCheck ($messageId)
    {
        //подключаем клиент для отправки смс
        $client = new Client();

        //API сервиса для проверки статуса
        $url = 'https://api.bytehand.com/v1/status';

        //параметры GET запроса
        $params = array(
            'id' => Yii::$app->params['sms']['AccountId'],
            'key' => Yii::$app->params['sms']['AccountKey'],
            'message' => $messageId
        );

        $url = $url . '?' . http_build_query($params);

        //ответ
        $response = $client->createRequest()
            ->setMethod('get')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($url)
            ->send();

        if ($response->isOk) {
            //{"status":0,"description":"205939340082042979"}
            //в случае успеха возвращаем статус сообщения
            return Json::decode($response->content, false)->description;
        } else {
            throw new ServerErrorHttpException('Status check failed.');
        }
    }

    /**
     *  Функция проверки баланса аккаунта сервиса СМС
     *  Можно будет вывести в админке
     */

    public static function getBalance ()
    {
        //подключаем клиент для получения текущего баланса
        $client = new Client();

        //API сервиса для отправки
        $url = 'https://api.bytehand.com/v1/balance';

        //параметры GET запроса
        $params = array(
            'id' => Yii::$app->params['sms']['AccountId'],
            'key' => Yii::$app->params['sms']['AccountKey'],
        );

        $url = $url . '?' . http_build_query($params);

        //ответ
        $response = $client->createRequest()
            ->setMethod('get')
            ->setFormat(Client::FORMAT_JSON)
            ->setUrl($url)
            ->send();

        if ($response->isOk) {
            //{"status":0,"description":"205939340082042979"}
            //в случае успеха возвращаем баланс
            return Json::decode($response->content, false)->description;
        } else {
            throw new ServerErrorHttpException('Get balance failed.');
        }
    }
}
