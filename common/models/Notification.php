<?php
namespace app\common\models;

use Yii;
use yii\httpclient\Client;
use yii\web\ServerErrorHttpException;

/**
 * model for sending push notifications
 * uses firebase
 * 
 * Class Notification
 * @package app\common\models
 */
class Notification
{
    private static $sendApiUrl = 'https://fcm.googleapis.com/fcm/send';

    /**
     * @param $clientIds - array of client's device token
     * @param $notificationParams
     *
     * $notificationParams => [
     *    'notification' => [
     *      'title' => 'notification title',
     *      'body' => 'notification text',
     *      'click_action' => 'action on notification click'
     *    ],
     *    'data' => [
     *
     *     ]
     * ]
     * 
     * @return mixed
     * @throws ServerErrorHttpException
     */
    public static function send($clientIds, $notificationParams)
    {
        $httpClient = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        $notificationParams['registration_ids'] = $clientIds;

        $response = $httpClient->post(self::$sendApiUrl, $notificationParams, self::headers())->setFormat(Client::FORMAT_JSON)->send();

        if ($response->isOk) {
            return json_decode($response);
        } else return false;
    }

    /**
     * returns array of headers for sending post to firebase api server
     * @return array
     */
    private static function headers()
    {
        return [
            'Content-Type'  => 'application/json',
            'Authorization' => 'key=' . Yii::$app->params['firebaseApiKey']
        ];
    }
}