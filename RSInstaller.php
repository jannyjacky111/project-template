<?php

echo "\033[32mCreate admin? (y\\n) \e[0m\n";
$is_admin = readline();

while ($is_admin != 'y' && $is_admin != 'n') {
    echo "\033[31m Press y or n \033[0m\n";
    $is_admin = readline();
}

if ($is_admin == 'y') {
    shell_exec('cp -r templates/admin admin');
    shell_exec('cp templates/config/config-admin.php config/config-admin.php');
    mkdir('web/admin/assets', 0777, true);
    shell_exec('cp templates/.htaccess web/admin/.htaccess');
    $content = file_get_contents('templates/index.php');
    $content = preg_replace('/front/', 'admin', $content);
    file_put_contents('web/admin/index.php', $content);
    echo "\033[32mAdmin created \e[0m\n";
}

echo "\033[32mEnter the api modules names separated by commas (besides public) \e[0m\n";
$apiFolders = readline();
$apiFolders = explode(',', $apiFolders);
$files = array_merge(
    array_map(function($val) {
        return 'templates/api/template/v1/controllers/'.$val;
    }, scandir('templates/api/template/v1/controllers')),
    array_map(function($val) {
        return 'templates/api/template/v1/models/'.$val;
    }, scandir('templates/api/template/v1/models')),
    [
        'templates/api/template/v1/Module.php',
        'templates/api/template/v1/paths.php',
        'templates/config/config-api-apitemplate.php'
    ]
);
foreach ($apiFolders as $apiFolder) {
    $apiFolder = trim($apiFolder);
    $className = strtoupper(substr($apiFolder, 0, 1)).substr($apiFolder, 1);
    mkdir('api/'.$apiFolder);
    mkdir('api/'.$apiFolder.'/v1');
    mkdir('api/'.$apiFolder.'/v1/controllers');
    mkdir('api/'.$apiFolder.'/v1/models');
    shell_exec('cp -r templates/api/template/v1/tests api/'.$apiFolder.'/v1/tests');
    
    foreach ($files as $file) {
        if (!stripos($file, '.php')) continue;
        $content = file_get_contents($file);
        $content = preg_replace(['/ApiTemplate/', '/apitemplate/'], [$className, $apiFolder], $content);
        file_put_contents(
            preg_replace(['/ApiTemplate/', '/apitemplate/', '/templates.api.template/s', '/templates.config/s'], 
                [$className, $apiFolder, 'api/'.$apiFolder, 'config'], $file), $content);
    }

    mkdir('web/api/'.$apiFolder);
    shell_exec('cp templates/.htaccess web/api/'.$apiFolder.'/.htaccess');
    $content = file_get_contents('web/api/public/index.php');
    $content = preg_replace('/public/', $apiFolder, $content);
    file_put_contents('web/api/'.$apiFolder.'/index.php', $content);
}

mkdir('web/files');
mkdir('api/pub/v1/models');


